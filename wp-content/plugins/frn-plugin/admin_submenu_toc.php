<?php

//Changes:
	// v1.1 - 1/31/18:
	//		changed advanced options "show" link from a plus sign to the text like related posts has.
	//		fixed table TD issues for advanced options
	//		fixed IDs to be unique for each field
	// v1.2 - 6/27/18 - added placeholder text for TOC default styling settings
	


// This PHP file adds the TOC options SUB menu page to the main FRN Settings Menu
add_action('admin_menu', 'frn_plugin_subpage_toc');
function frn_plugin_subpage_toc() {
	add_submenu_page( 'frn_features',"FRN Automated Table of Contents Options", "Table of Contents", 'manage_options', 'frn_toc', 'frn_subpage_toc_intro');
	add_settings_section('frn_toc_shortcode', 'Shortcode', '', 'frn_toc');
		add_settings_field('frn_toc_sc_info', "", 'frn_toc_sc_info', 'frn_toc', 'frn_toc_shortcode');
	add_settings_section('frn_toc_auto', "Automated Options<a href='javascript:showhide(\"frn_plugin_toc_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a>", '', 'frn_toc');
		add_settings_field('frn_auto_toc_act', '
			<div style="margin-right: 35px;>
				<small>
				Tip: <span style="font-weight:normal">
						Install <a href="https://www.advancedcustomfields.com/my-account/" target="_blank">Advanced Custom Fields PRO plugin</a> for post-specific overrides. 
						You will need Jonny Stovall\'s login to gain access to the zip file you\'ll install. <a href="'.$GLOBALS['frn_acf_start'].'">Download and import this file</a> to install the custom fields used by this plugin.
					</span>
				</small>
			</div>', 'frn_auto_toc_act', 'frn_toc', 'frn_toc_auto'
		);
	
	add_action('admin_init', 'frn_toc_variable');
}

function frn_toc_variable() {
	//registers the variable for the database
	register_setting( 'frn_plugin_toc', 'site_toc','plugin_options_auto_toc_save'  ); 
}



function frn_subpage_toc_intro() {
	?>
	<div class="wrap">
		<h2>FRN Plugin: Automated Table of Contents</h2>
		<div class="intro_text" style="margin-top:50px;">
			<p>This tool will scan all the HTML and text you add to a main content field for pages and posts and look for section headers. If at least two are found, it'll build a table of those headers linked to each section.</p>
			<p>You can automatically add the TOC to the top of content using these settings. Or you can rely on the shortcode to place it. If there is an image in the first 400 characters of the HTML, then it'll move it to the second section header and leave the TOC at the top. You can change various features. It comes with a standard styling, but you can disable those and create your own CSS in the site's CSS file.</p>
			
			<br />
			<hr />
			<br />
		</div>
		<form action="options.php" method="post" class="frn_styles">
			<?php
				settings_fields( 'frn_plugin_toc' );
				do_settings_sections( 'frn_toc' );
			?>
			<table class="form-table">
				<tr>
					<th></th>
					<td>
						<input name="Submit" type="submit" class="button button-primary" value="<?php esc_attr_e('Save Changes'); ?>" />
					</td>
				</tr>
			</table>
			</div>
		</form>
	</div>
	<?php 
}



////////
// Auto_TOC Settings
/////////

//The following includes settings for creating a table of contents list of section headers at the top of content.
//Primary benefits is that Google will add these as sub-links in search results when a page shows. That increases attention and provides more keywords that relate to what someone really wants to know.
//The second benefit is for mobile users. It keeps them from having to scroll as much and reduces reader fatique.


function frn_toc_sc_info() {
	?>
	<div class="frn_options_table"><table class="frn_options_table"><tr>
			<td valign="top">Shortcode: </td>
			<td valign="top"><b><span id="frn_plugin_toc_sc" class="frn_shortcode_sel" onClick="selectText('frn_plugin_toc_sc')">[frn_toc]</span></b> <small> (Only use in content, not PHP.)</small></td>
		</tr><tr>
			<td valign="top">PHP Option: </td>
			<td valign="top">You cannot print the TOC separate from content using PHP since page anchors also need to be added. But you can automatically add a TOC as long as you plan to echo the content. You can even add ACF fields to the content so the TOC analyzes all headers. <b><span id="frn_plugin_toc_php" class="frn_shortcode_sel" onClick="selectText('frn_plugin_toc_php')">&lt;?php if(function_exists('frn_auto_toc')) echo frn_auto_toc($content);?&gt;</span></b></td>
	</tr></table></div>
	<?php
}

function frn_auto_toc_act() {

	$toc=get_option('site_toc');
	if(!isset($toc['activation'])) $toc['activation']="";



	
	////////
	// Activation
	?>
	<input type="radio" name="site_toc[activation]" value="yes" <?=($toc['activation']=="yes") ? "checked" : null;?> onClick="showhide('frn_auto_toc_wrap','block')" /> Activate <br />
	<input type="radio" name="site_toc[activation]" value="no" <?=($toc['activation']=="no" || $toc['activation']=="") ? "checked" : null;?> onClick="showhide('frn_auto_toc_wrap','none')" /> Deactivate (Default) <br />
	<input type="radio" name="site_toc[activation]" value="sc" <?=($toc['activation']=="sc") ? "checked" : null;?> onClick="showhide('frn_auto_toc_wrap','none')" /> Remove Shortcode <br />
	<div id="frn_plugin_toc_hlp" class="frn_help_boxes" style="display:none;">
		<b>IMPORTANT</b>
		<ul class="frn_level_1">
			<li><b>Why Should You Use This? </b>
				<ol class="frn_level_2">
					<li>This increases the amount of links showing in Google search results and can better answer searcher's questions. Google studies have proved this feature valuable for click through rates.</li>
					<li>It is helpful to users to more quickly get their answers and reduces reader fatigue.</li>
					<li>It's most valuable for smartphone users by helping them get to what they're interested in faster and significantly reduces fatique. </li>
				</ol>
			</li>
			<li><b>Content Hook:</b> This feature "hooks" into the content feature of WordPress. Whatever is there will be analyzed.</li>
			<li><b>Plugins Affect This: </b>If a plugin adds content to the content you enter into the content field for posts or shortcodes a anything using the header level you selected, that item will be added to the list in the TOC. There won't be a way to exclude those items. You'd have to modify their code to change the behavior.</li>
			<li><b>Remove Shortcode: </b>The shortcode above is not a traditional shortcode since we need to analyze content after shortcodes are executed. Instead, this feature merely finds and replaces the shortcode with the TOC. That's the reason for the additional "remove shortcode" activation option.</li>
			<li><b>Types Excluded: </b>By default, the following page types will be ignored.
				<ul class="frn_level_2">
					<li>Front Page</li>
					<li>Home Page</li>
					<li>Listing pages (e.g. blog homepage, tags, and categories)</li>
					<li>Contact Page (requires "contact" to be in the title)</li>
					<li>Attachment pages</li>
					<li>Revisions</li>
					<li>Nav_menu_item page types</li>
				</ul>
			</li>
		</ul>
	</div>
	<?php




	////////
	// Shortcode
	if(!isset($toc['sc_only'])) $toc['sc_only']="";
	?>
	<div id="frn_auto_toc_wrap" style="display:<?=($toc['activation']=="yes" || $toc['sc_only']!=="") ? 'block' : 'none' ;?>">
		

		<h3 style="font-size: 15px;margin-bottom: 0;">Shortcode: <b>[frn_toc]</b> <a href="javascript:showhide('frn_plugin_toc_hlp_sc')" ><img src="<?=$GLOBALS['help_image'];?>" /></a></h3>
		<p><input type="checkbox" name="site_toc[sc_only]" value="yes" <?=($toc['sc_only']!=="") ? "checked" : null ;?> onClick="showhide('frn_auto_toc_pages_wrap')" /> Shortcode Only Mode </p>
		<div id="frn_plugin_toc_hlp_sc" class="frn_help_boxes" style="display:none;">
			<b>Shortcode Help</b>
			<ul class="frn_level_1">
				<li><b>Custom Positioning:</b> Insert this into the traditional WP content field in order to specifically position the table of contents where you want it.</li>
				<li><b>Deactivating Sitewide:</b> 
					<ul class="frn_level_2">
						<li><b>Fake Shortcode:</b> The shortcode above is not a traditional shortcode since we need to analyze content after shortcodes are executed. </li>
						<li><b>Find/Replace FRN_TOC:</b> Instead, the FRN Plugin TOC feature merely finds and replaces the text that looks like a shortcode with the TOC. This is achieved by using a regular expression in a preg_replace PHP function.</li>
						<li><b>Using in Unselected Post Types:</b> If you have a shortcode in a post and it's not one of those selected below, the shortcode will be stripped automatically when the content is delivered. The only way to make it work is by including the post ID in the "Use Only On" setting.</li>
						<li><b>Remove Shortcode Option:</b> The "Remove Shortcode" activation option above is necessary if you won't want to search all posts for the shortcode to manually remove them. The system will shut everything down but still find and remove the frn_toc shortcode. So if you simply select "Deactivate", our fake shortcode will remain.</li> 
					</ul>
				</li>
				<li><b>Shortcode Only Mode:</b> 
					<ul class="frn_level_2">
						<li>If you want to control things very manually, activate this option. It will shutdown all automatic placement features and only place TOCs on pages with the shortcode. </li>
						<li>The shortcode will NOT run on listing pages, the front page (typically blog page), homepage, attachments, revisions, and menus. It will only run on "post" or "page" types--that includes custom post types with that declaration.</li>
						<li>You could still achieve this with the other settings by not selecting any post types or included IDs and adding the shortcode, but that's a lot of unnecessary system processing for the same result.</li>
						<li>If you discontinue this feature, be sure to remove the shortcode from all posts or select the "Remove Shortcode" activation option and the system strip it out before delivering content.</li>
					</ul>
				</li>
			</ul>
		</div>
	
		<?php //first div for entire options based on activation ?>
		<div id="frn_auto_toc_pages_wrap" style="display:<?=($toc['sc_only']!=="") ? 'none' : 'block' ;?>">
			<?php //second div to control if shortcode only option to hide unrelated ?>
			<?php



			////////
			// Post Types

			if(!isset($toc['post_types'])) $toc['post_types']="";
			?>
		
			<h3 style="font-size: 15px;">Use Automatically On Post Types: <a href="javascript:showhide('frn_plugin_toc_hlp_pt')" ><img src="<?=$GLOBALS['help_image'];?>" /></a></h3>
			<?php //print_r($toc['post_types']);?>
			<div>
			<?php
				$args=array(
					'public'   => true,
					);
				$post_types = get_post_types( $args, 'names' ); //'objects'
				if($post_types) {
					foreach($post_types as $post_type) {
						if($post_type!=="attachment" && $post_type!=="revision" && $post_type!=="nav_menu_item") { 
							$selected="";
							if(is_array($toc['post_types'])) {
								foreach($toc['post_types'] as $selected_type) {
									if($selected=="") {if($post_type==$selected_type) $selected="checked";}
								}
							}
			?>
				<div style="float:left; width:33%; min-width:100px;" ><input type="checkbox" name="site_toc[post_types][]" value="<?=$post_type; ?>" <?=$selected;?> /> <?=$post_type; ?></div>
			<?php
						}
					}
				}
				else echo "<p>No \"Public\" Post Types Found</p>";
			?>
			</div>
			<div id="frn_plugin_toc_hlp_pt" class="frn_help_boxes" style="display:none; float: left; width: 100%;">
				<b>Post Types Help</b>
				<ul class="frn_level_1">
					<li>This lists all the "public" post types on the site.</li>
					<li>If a type isn't above an it should be, make sure it's set to be public when registering the post type in the functions.php file (or wherever the function is).</li>
					<li>Select all the post types you want TOCs automatically added.</li>
					<li>Only when you select a post type will the "exclude" IDs be activated.</li>
					<li>The "include" IDs is activated whether or not you select a post type. Of course, if the included post ID's type is also the same as the selected post type, then it's redundant.</li>
				</ul>
			</div>

		</div><?php //end wrap to hide features when in shortcode only mode?>
		<?php 




		////
		// Advanced Options

		if(!isset($toc['class'])) $toc['class']="";
		if(!isset($toc['styles'])) $toc['styles']="";
		$checked=""; $checked_no=""; $checked_yes="";
		if($toc['styles']=="yes" || $toc['styles']=="") $checked_yes=" checked"; //default
		if($toc['styles']=="no") $checked_no=" checked";
		if(!isset($toc['id'])) $toc['id']="";
		if(!isset($toc['h_level'])) $toc['h_level']="";
		if(!isset($toc['title_desktop'])) $toc['title_desktop']="";
		if(!isset($toc['title_mobile'])) $toc['title_mobile']="";
		if(!isset($toc['chars'])) $toc['chars']="";
		if(!isset($toc['menu_top_size'])) $toc['menu_top_size']="";

		?>
		<h3 style="font-size: 15px; float: left; width: 100%;">Advanced Options (<a href="javascript:showhide('frn_plugin_toc_adv')" >show</a>) <a href="javascript:showhide('frn_plugin_toc_hlp_opt')" ><img src="<?=$GLOBALS['help_image'];?>" /></a></h3>
		<div id="frn_plugin_toc_hlp_opt" class="frn_help_boxes" style="display:none;">
			<b>Table Options Help</b>
			<ul class="frn_level_1">
				<li><b>Notes: </b> 
					<ul class="frn_level_2">
						<li>Customize a link in the TOC by simply adding <span class="frn_shortcode_sel">title=""</span> to the selected header level. e.g. <span style="white-space:nowrap;">&lt;h2 <b>title="Shorter Title"</b> &gt;</span>Original Header&lt;/h2&gt;</li>
						<li>To skip a header and keep it from showing in the TOC, use skip (or remove or delete) as the "title" instead: <span class="frn_shortcode_sel"> title="skip"</span>
						<li>The TOC is essentialy an undefined list wrapped in a DIV to simplify styling and responsive capability.</li>
						<li><b>Customized Styling:</b> Customize the TOC display for specific posts by using the FRN Plugin Post Options at the bottom of any page's or post's edit page.</li>
					</ul>
				</li>
				<li><b>Scan Header Level:</b> Choose the H level you want to scan the page for. At this time, you can only scan one level. If a page includes H2's and H3's, you have to choose one of them to create anchors for.</li>
				<li><b>Image Scan Threshold:</b> 
					<ul class="frn_level_2">
						<li><b>Purpose:</b> The system makes sure an image won't conflict with adding a TOC. If it might, then it is moved to just before the first or second section header--which ever is also not within the threshold. </li>
						<li><b>Shortcode Override:</b> If the shortcode is in content, this feature shuts down under the assumption the content developer won't place an image or shortcode somewhere that they'll conflict.</li>
						<li><b>Smartphone Deactivation:</b> This feature does not apply for smartphones--under the assumption that both the TOC and images will be 100% and the distance between them will be too great to cause a problem (i.e. look unprofessional).</li>
					</ul>
				</li>
				<li><b>Default Styling:</b> To save time, included is default styling. It's not fully fleshed out, but it gives you a quick option. This adds a "STYLE" section just before the table so that it can be responsive.</li>
				<li><b>Custom Class:</b> The default class for the table is <b>frn_toc</b>. However, you can overwrite this with your own by including it here. Whatever you enter here will display exactly the same in the CLASS attribute for the DIV wrapper of the list.</li>
				<li><b>Custom ID:</b> There is no default ID for the table. But if you wanted to do something more dramatic with the styling or use traditional JS to interact with the table, you can enter an ID here. Whatever you enter here will display exactly the same in the ID attribute for the DIV wrapper of the list.</li>
				<li><b>Smartphones Title:</b> The default title is "Jump to Section:". The user interaction is dramatically different from a smartphone and a tablet/desktop. Due to the table being more prominent on smartphones, the feature becomes an easy way to help people get to the section they care about. Use "empty" in the field if you don't want one included.</li>
				<li><b>Desktops Title:</b> The default title is "On This Page". Users can easily overlook the table on wider screens. They can also take in more information in one glance, easily understanding context. As a result, the purpose the title may need to be different. Use "empty" in the field if you don't want one included. </li>
				<li><b>CSS Styles:</b> The remaining fields for margin, padding, etc. are to be entered just like you would when styling with CSS. For example, margin: 15px 5px 15px 0. Ask a designer if you need help or use a browser's "Inspect Element" to play with CSS until you get the look you want for the table of contents.</li>
			</ul>
		</div>
		<div id="frn_plugin_toc_adv" style="display:none; border: 2px solid #e3e3e3; padding: 0 10px; float: left; width: 100%;">
			<?php



			////////
			// Images in Content
			if(!isset($toc['image'])) $toc['image']="";
			?>
			<h3 style="font-size: 15px; float: left; width: 100%; margin-top: 10px;">How are images added into content? <a href="javascript:showhide('frn_plugin_toc_hlp_sp')" ><img src="<?=$GLOBALS['help_image'];?>" /></a></h3>
			<select name="site_toc[image]">
				<option value="both" <?=($toc['image']=="" || $toc['image']=="both") ? "selected" : null ;?>>Show TOC regardless (default)</option>
				<option value="content" <?=($toc['image']=="content") ? "selected" : null ;?>>Manually only</option>
				<option value="featured" <?=($toc['image']=="featured") ? "selected" : null ;?>>Only via the featured images option (not as a banner)</option>
				<option value="present" <?=($toc['image']=="present") ? "selected" : null ;?>>Disable TOC if image is at the top of content</option>
				<!--<option value="disabled" <?=($toc['image']=="disabled") ? "selected" : null ;?>>Manually and via featured image option</option>-->
			</select><br />
			<div id="frn_plugin_toc_hlp_sp" class="frn_help_boxes" style="display:none;">
				<b>Images Help</b>
				<ul class="frn_level_1">
					<li>To avoid positioning conflicts between the TOC and images and to save processing load, it helps if the FRN Plugin knows where to look for images. Often images are left aligned at the top of content. But that's where the TOC should be. So the plugin will move the image or change where the TOC is positioned as is appropriate. You can always override this by using the shortcode instead.</li>
					<li><b>Manually Inserted: </b>Most of our sites use this option. We simply insert an image using the typical WP inserting option. The image code goes direclty into the content.</li>
					<li><b>Featured Images: </b>It's rare, but sometimes featured images are displayed just like inserted images. They show at the top of content and are left or right aligned. </li>
					<li><b>Both: </b>It's rare, but sometimes featured images are displayed just like inserted images. They show at the top of content and are left or right aligned. </li>
					<li><b>Disable When Present: </b>It's rare, but sometimes featured images are displayed just like inserted images. They show at the top of content and are left or right aligned. </li>
					<li><b>Disabled Completely: </b>If you know this feature isn't working for this site, but you still want to use the auto insert feature, just disable it's ability to look for images.</li>
				</ul>
			</div>
			<h3 style="font-size: 15px; float: left; width: 100%; margin-top: 10px;">Device-Specific Display CSS <a href="javascript:showhide('frn_plugin_toc_hlp_device')" ><img src="<?=$GLOBALS['help_image'];?>" /></a></h3>
			<?php
				if(!$toc['desktab']) $toc['desktab']="show-for-medium"; 
				if(!$toc['desktop']) $toc['desktop']="show-for-large"; 
				if(!$toc['tablet']) $toc['tablet']="show-for-medium-only"; 
				if(!$toc['mobile']) $toc['mobile']="show-for-small-only"; 
			?>
			<table class="frn_options_table">
				<tr>
					<td>Only Desktop & Tablet: </td><td><input id="frn_toc_dt" name="site_toc[desktab]" size="30" type="text" value="<?=$toc['desktab'];?>" /></td>
				</tr>
				<tr>
					<td>Only Desktop: </td><td><input id="frn_toc_desk" name="site_toc[desktop]" size="30" type="text" value="<?=$toc['desktop'];?>" /></td>
				</tr>
				<tr>
					<td>Only Tablet: </td><td><input id="frn_toc_tab" name="site_toc[tablet]" size="30" type="text" value="<?=$toc['tablet'];?>" /></td>
				</tr>
				<tr>
					<td>Only Mobile: </td><td><input id="frn_toc_mob" name="site_toc[mobile]" size="30" type="text" value="<?=$toc['mobile'];?>" /></td>
				</tr>
			</table>
			<div id="frn_plugin_toc_hlp_device" class="frn_help_boxes" style="display:none;">
				<b>Images Help</b>
				<ul class="frn_level_1">
					<li><b>Only Desktop & Tablet:</b> This option should cover screen sizes starting at 768px. The Foundation CSS framework (default) begins at 641px (show-for-medium). At 641px, the TOC will still show for some smartphones in landscape position (all modern smartphones).</li>
					<li><b>Only Desktop:</b> This option should cover screen sizes starting at 1024px. The Foundation CSS framework (default) begins at 1025px (show-for-large). This option means the TOC will still show for large tablets like the iPad Pro and most modern tablets in the landscape position.</li>
					<li><b>Only Tablet:</b> This option should cover screen sizes between 768px and 1025px. The Foundation CSS framework (default) is from 641px to 1024px (show-for-medium-only). That will include all tablets, all modern smartphones in landscape, and small laptops with the browser at their max window size.</li>
					<li><b>Only Mobile:</b> This option should cover screen sizes up to 767px. The Foundation CSS framework (default) tops out at 640px (show-for-small-only). That means that modern smartphones in landscape mode would not see the TOC if this is set.</li>
					<li><b>Resources:</b> If you'd like to see one resource that lists the viewport screen sizes, go to <a href="https://mediag.com/blog/popular-screen-resolutions-designing-for-all/">mediag.com</a>. Foundation CSS framework info is in the video on <a href="https://foundation.zurb.com/sites/docs/visibility.html">this page</a>.</li>
				</ul>
			</div>
			<?php



			////////
			// Specific Pages
			if(!isset($toc['include'])) $toc['include']="";
			if(!isset($toc['exclude'])) $toc['exclude']="";
			?>
			<h3 style="font-size: 15px; float: left; width: 100%;">Specific Pages: <a href="javascript:showhide('frn_plugin_toc_hlp_sp')" ><img src="<?=$GLOBALS['help_image'];?>" /></a></h3>
			<table class="frn_options_table">
				<tr>
					<td>Use Only In: </td><td><input id="frn_toc_incl" name="site_toc[include]" size="30" type="text" value="<?=$toc['include'];?>" /></td>
				</tr>
				<tr>
					<td>DO NOT Use In: </td><td><input id="frn_toc_excl" name="site_toc[exclude]" size="30" type="text" value="<?=$toc['exclude'];?>" /></td>
				</tr>
			</table>
			<div id="frn_plugin_toc_hlp_sp" class="frn_help_boxes" style="display:none;">
				<b>Specific Pages Help</b>
				<ul class="frn_level_1">
					<li><b>Use Only In: </b>
					<ul class="frn_level_2">
						<li>Use a comma seperated list of post IDs where you specifically want the TOC to show. </li>
						<li>Including even one ID will override any post type selection above.</li>
						<li>Sometimes you need to add a post ID to this list if (1) a page uses the shortcode, (2) it's not a selected post type, and (3) shortcode only mode above is not activated.</li>
					</ul></li>
					<li><b>DO NOT Use In: </b>
					<ul class="frn_level_2">
						<li>Use a comma seperated list of post IDs where you specifically don't want the TOC to show. </li>
						<li>This is only activated if you have selected post types above.</li>
					</ul></li>
				</ul>
			</div>

			<table class="frn_options_table">
				<tr>
					<td style="width: 105px;">Scan Header Level:</td>
					<td><select id="frn_toc_hdr" name="site_toc[h_level]">
						<option value="h1" <?=($toc['h_level']=="h1") ? "selected" : null ;?>>H1</option>
						<option value="h2" <?=($toc['h_level']=="h2" || $toc['h_level']=="") ? "selected" : null ;?>>H2 (Default)</option>
						<option value="h3" <?=($toc['h_level']=="h3") ? "selected" : null ;?>>H3</option>
					</select></td>
				</tr>
				<tr>
					<td style="width: 105px;">Image Scan Threshold:</td>
					<td>
						<input id="frn_toc_chars" name="site_toc[chars]" size="10" type="text" value="<?=$toc['chars'];?>" placeholder="400" /><small> (characters)</small>
						<p><small><b style="color:red;">TIP:</b> Customize TOC link by adding <b>title=""</b> as an attribute to a H2 tag.</small></p>
					</td>
				</tr>
			</table>
			 
			<table class="frn_options_table" style="width: 100%;" >
				<tr>
					<td>Custom Class: </td><td><input id="frn_rp_title" name="site_toc[class]" size="30" type="text" value="<?=$toc['class'];?>" placeholder="frn_toc" /></td>
				</tr><tr>
					<td>Custom ID: </td><td><input id="frn_toc_id" name="site_toc[id]" size="30" type="text" value="<?=$toc['id'];?>" /></td>
				</tr><tr>
					<td>Smartphones Title: </td><td><input id="frn_toc_mtitle" name="site_toc[title_mobile]" size="30" type="text" value="<?=$toc['title_mobile'];?>" placeholder="Jump to Section:" /></td>
				</tr><tr>
					<td>Desktops Title: </td><td><input id="frn_toc_dtitle" name="site_toc[title_desktop]" size="30" type="text" value="<?=$toc['title_desktop'];?>" placeholder="On This Page:" /></td>
				</tr><tr>
					<td>Default Styling: </td><td><input type="radio" name="site_toc[styles]" value="yes" <?=$checked_yes;?> onClick="showhide('frn_css_overrides','block');" /> Yes &nbsp; <input type="radio" name="site_toc[styles]" value="no" <?=$checked_no;?>  onClick="showhide('frn_css_overrides','none');" /> No </td>
				</tr><tr>
					<td colspan="2">
						<?php 
							if($toc['styles']=="no") {
								$css_overrides="none";
							}
							else {
								$css_overrides="block";
							}
						?>
						<table class="frn_options_table" id="frn_css_overrides" style="display:<?=$css_overrides;?>; border: 2px solid #e3e3e3; margin-top: 15px; width:100%;">
							<tr style="background-color:#e3e3e3;">
								<td style="padding-bottom:8px; font-weight:bold;">CSS Customization: </td>
								<td></td>
							</tr><tr>
								<td>&nbsp; Alignment: </td>
								<td>float: <select id="frn_toc_hdr" name="site_toc[float]">
										<option value="left" <?=($toc['float']=="left" || $toc['float']=="") ? "left" : null ;?>>Left (default)</option>
										<option value="right" <?=($toc['float']=="right") ? "right" : null ;?>>Right</option>
										<option value="full" <?=($toc['float']=="full") ? "full" : null ;?>>Full Width</option>
										<option value="none" <?=($toc['float']=="none") ? "none" : null ;?>>None</option>
									</select>;
								</td>
							</tr><tr>
								<td>&nbsp; margin: </td>
								<td><input id="frn_toc_margin" name="site_toc[margin]" size="30" type="text" value="<?=$toc['margin'];?>" placeholder="0 15px 20px 0" />; </td>
							</tr><tr>
								<td>&nbsp; padding: </td>
								<td><input id="frn_toc_padding" name="site_toc[padding]" size="30" type="text" value="<?=$toc['padding'];?>" placeholder="0" />;</td>
							</tr><tr>
								<td>&nbsp; width: </td>
								<td><input id="frn_toc_width" name="site_toc[width]" size="30" type="text" value="<?=$toc['width'];?>" placeholder="inherit" />;</td>
							</tr><tr>
								<td>&nbsp; max-width: </td>
								<td><input id="frn_toc_mwidth" name="site_toc[maxwidth]" size="30" type="text" value="<?=$toc['maxwidth'];?>" placeholder="50%" />;</td>
							</tr><tr>
								<td>&nbsp; border: </td>
								<td><input id="frn_toc_border" name="site_toc[border]" size="30" type="text" value="<?=$toc['border'];?>" placeholder="1px solid #ccc" />;</td>
							</tr><tr>
								<td>&nbsp; border-radius: </td>
								<td><input id="frn_toc_radius" name="site_toc[border_radius]" size="30" type="text" value="<?=$toc['border_radius'];?>" placeholder="10px" />;</td>
							</tr><tr>
								<td>&nbsp; background: </td>
								<td><input id="frn_background" name="site_toc[background]" size="30" type="text" value="<?=$toc['background'];?>" placeholder="none" />;</td>
							</tr><tr>
								<td>&nbsp; list-style: </td>
								<td><select id="frn_toc_list" name="site_toc[list_style]">
										<option value="none" <?=($toc['list_style']=="none" || $toc['list_style']=="") ? "none" : null ;?>>None (default)</option>
										<option value="disc" <?=($toc['list_style']=="disc") ? "disc" : null ;?>>Disc</option>
										<option value="circle" <?=($toc['list_style']=="circle") ? "circle" : null ;?>>Circle</option>
										<option value="numbers" <?=($toc['list_style']=="numbers") ? "numbers" : null ;?>>Numbers</option>
									</select>;
								</td>
							</tr><tr>
								<td>&nbsp; UL margin: </td>
								<td><input id="frn_toc_ulmargin" name="site_toc[ul_margin]" size="30" type="text" value="<?=$toc['ul_margin'];?>" placeholder="0" />;</td>
							</tr><tr>
								<td>&nbsp; UL padding: </td>
								<td><input id="frn_toc_ulpadding" name="site_toc[ul_padding]" size="30" type="text" value="<?=$toc['ul_padding'];?>" placeholder="0" />;</td>
							</tr><tr>
								<td>&nbsp; Stationary Top Menu Height: </td>
								<td><input id="frn_toc_menu_top" name="site_toc[menu_top_size]" size="30" type="text" value="<?=$toc['menu_top_size'];?>" placeholder="60px" />;</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		
		
			<?php



		
			////////
			// Related Pages

			if(!isset($toc['addl_anchor'])) $toc['addl_anchor']="";
			if(!isset($toc['addl_title'])) $toc['addl_title']="";
			if(!isset($toc['addl_link'])) $toc['addl_link']="";
			if(!isset($toc['rp_frn'])) $toc['rp_frn']="";
			if(!isset($toc['post_types_rp'])) $toc['post_types_rp']="";
			$checked=""; $checked_frnrp="";
			if($toc['addl_link']!=="") $checked=" checked";
			if($toc['rp_frn']!=="") $checked_frnrp=" checked";
			?>
			<h3 style="font-size: 15px;">Related Pages Anchor: <a href="javascript:showhide('frn_plugin_toc_hlp_rp')" ><img src="<?=$GLOBALS['help_image'];?>" /></a></h3>
			<div style="margin-left:10px; margin-bottom:10px;">
				<input type="checkbox" name="site_toc[rp_frn]" value="Y" <?=$checked_frnrp;?> /> Ignore FRN Plugin Related Posts<br /> 
				<input type="checkbox" name="site_toc[addl_link]" value="Y" <?=$checked;?> onClick="showhide('frn_auto_toc_rp');" /> Add another TOC link?
			</div>
			<div id="frn_auto_toc_rp" style="display:<?=($toc['addl_link']!=="") ? 'block' : 'none' ;?>;" >
				<div style="margin-bottom:5px;">On What Page Types: </div>
				<div>
					<?php //print_r($toc['post_types']);?>
					<?php
						$args=array('public' => true);
						$post_types = get_post_types( $args, 'names' ); //'objects'
						if($post_types) {
							foreach($post_types as $post_type) {
								if($post_type!=="attachment" && $post_type!=="revision" && $post_type!=="nav_menu_item") { 
									$selected="";
									if(is_array($toc['post_types_rp'])) {
										foreach($toc['post_types_rp'] as $selected_type) {
											if($selected=="") {if($post_type==$selected_type) $selected="checked";}
										}
									}
					?>
						<div style="float:left; width:33%;" ><input type="checkbox" name="site_toc[post_types_rp][]" value="<?=$post_type; ?>" <?=$selected;?> /> <?=$post_type; ?></div>
					<?php
								}
							}
						}
						else echo "<p>No \"Public\" Post Types Found</p>";
					?>
				</div>
				<table class="frn_options_table">
					<tr>
						<td>Page Anchor: </td><td>#<input id="frn_rp_anchor" name="site_toc[addl_anchor]" size="30" type="text" value="<?=$toc['addl_anchor'];?>" /></td>
					</tr><tr>
						<td>Link in TOC: </td><td><input id="frn_rp_title" name="site_toc[addl_title]" size="30" type="text" value="<?=$toc['addl_title'];?>" /></td>
					</tr>
				</table>
			</div>
			<div id="frn_plugin_toc_hlp_rp" class="frn_help_boxes" style="display:none;">
				<b>Related Posts Anchor Help</b>
				<li><b>Why Should You Use This? </b> If the selected post types above have a related posts listed below the content, it is likely a good idea to include a link to them at the bottom  of the TOC. Keep in mind you can use any anchor and title the link whatever you want, so it doesn't have to be for related posts.</li>
				<li><b>Ignore FRN Plugin Related Posts:</b> By default, the TOC system executes after shortcodes are expanded. That means FRN's related posts will be seen when analyzing "the_content" filter. If you don't want a page anchor to it in the TOC, place a check in the checkbox here.</li>
				<li><b>Post Types: </b>
					<ul class="frn_level_2">
						<li>This lists all the "public" post types on the site. If a type isn't there an it should be, make sure it's set as public when registering the post type in PHP.</li>
						<li>Select all the post types that have related posts at the bottom of the content.</li>
						<li>As noted next, if you plan to use another page anchor, be sure to select the page type that the anchor is used on or the link will show in the TOC but not work.</li>
					</ul>
				</li>
				<li><b>More than RP: </b>This feature merely adds a link at the bottom of the TOC to any page anchor you'd like. It's called Related Posts to get us thinking, but you don't have to limit yourself to that.</li>
				<li><b>Post Types:</b> Select the post types that have related posts below them.</li>
				<li><b>Page Anchor:</b> Include the page anchor you've programmed into the template. This feature only scans content and no other part of the page.</li>
				<li><b>Link in TOC:</b> You can customize the text linked for the related posts anchor. </li>
				<!--<li><b>Caching: </b>Using a search every time a page is loaded is pretty resource heavy. Caching reduces how often that happens. By default, the cache refreshes for each page <b>once a day</b>. If you added a new post and you want it to show as a related post, disable the cache. The cache will only refresh when a page is loaded after the four hour timeframe. If you'd like to know more, read about <a href="https://codex.wordpress.org/Transients_API" target="_blank">WordPress's Transient feature</a>.</li>-->
			</div>
		<?php

		/*
		// Disable WP Cache
		// Relized this option isn't helpful
		// We still need to add page anchors. To do that, you need DOM processing activated.
		// Just storing the TOC code wouldn't reduce much of the load since the content objects still needed to be processed.
		// Deactivating, but keeping code in case there is an epiphany in the future on a light method for how we can utilize this and still process anchors.
		if(!isset($toc['cache'])) $toc['cache']=""; 
		$checked="";
		if($toc['cache']!=="") $checked=" checked";
		?>
			<h3 style="font-size: 15px;">Caching:</h3> 
			<input type="checkbox" name="site_toc[cache]" value="Y" <?=$checked; ?> /> Disable TOC Caching
		*/
		?>

		</div> <?php //end advanced options ?>
	</div> <?php //end toc wrapper for activation ?>

<?php
}
function plugin_options_auto_toc_save($input) {
	//trim spacing from use inputed values
	$input['chars']			=	trim($input['chars']);
	if($input['chars']==400) $input['chars']=""; //removing default to allow system wide changing of this if needed
	$input['include']		=	trim($input['include']);
	$input['exclude']		=	trim($input['exclude']);
	$input['class']			=	trim($input['class']);
	$input['id']			=	trim($input['id']);
	$input['title_mobile']	=	trim($input['title_mobile']);
	$input['title_desktop']	=	trim($input['title_desktop']);
	$input['addl_anchor']	=	trim($input['addl_anchor']);
	$input['addl_title']	=	trim($input['addl_title']);

	return $input;
}