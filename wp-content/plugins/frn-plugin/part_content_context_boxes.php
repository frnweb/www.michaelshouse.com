<?php defined( 'ABSPATH' ) OR die( 'No direct access.' );

/* NOTES:
	"part" php files are in the works in an attempt to make it easier to add to any theme without requiring the massive main plugin PHP file.
	Unlike most "parts", nothing else depends on these features
	This file references other elements like analytics, phone numbers, etc.
	This file only includes frontend changes for users. 
	Admin features are still in the admin php.
	Move to it's own file on 3/7/17


TABLE OF CONTENTS:
	Boxes_Shortcode (line: 3650)
	URL_Check (line: 4015)


*/





///////////////////////////////////////
// PERSONALIZATION and CONTEXT BOXES  //
///////////////////////////////////////

//add_shortcode( 'frn_boxes', 'frn_personal_function' );

//PURPOSE:
	//This function controls the AUTO INSERTING of our in-content context boxes.
	//It relies on the shortcode. 
	//So, all this does is determine where and what type of shortcode to place in the page's content.
	//Basically it inserts the shortcode into a DIV at the proper place in the length of a page. The shortcode function takes over from there. The function for that is after this one here.
	//When this code is added to a site for the first time, it'll automatically be disabled since nothing has been saved into the DB yet.
//TECHNICAL: 
	//This is a content hook since we have to analyze the content for word count or headers to place the CTA box at the right place on the page.
	//The wordpress "action" that activates/calls this function is just after this function.
	
/*
//////////////
//// GLOBAL VARIABLES (DEFAULTS)
/////////////
*/
	//Global enables us to use these variables across functions.
	//These will not work outside this page, however.
	//In the end, this makes it easier for defaults to be found and changed by us.
	//global $personal_box_var; global $frn_cta_mssg; global $frn_trigger_words;
	$GLOBALS['personal_box_var'] = "frn_personal_boxes"; //don't forget saving function too
	$GLOBALS['message_cta'] = "
					<div class=\"frn_cta_title\">
						Have questions about this %%drug%%? We've helped thousands since 1995 determine what direction they need to go.
					</div>
					<strong class=\"hide-for-small-only\">Confidential. No judgment. We're here to help.</strong>";
	$GLOBALS['trigger_words'] = "rehab, treatment, facility, center, outpatient, inpatient, counsel, intervention, recovery, emergency, get clean, withdrawal";
	
if(!function_exists('frn_incontent_boxes')) {
function frn_incontent_boxes($content) {
	
	/*
	//Restrict by page type
		See if value set for types of posts to show on
		if so, get registered post types
		Foreach and check against current post type
		if post type is true, then execute remaining code
	
	*/
	
	$all_box_options = get_option($GLOBALS['personal_box_var']);
	$core_shortcode = "frn_boxes";
	
	if(!isset($all_box_options['page_type'])) $all_box_options['page_type']="";
	if($all_box_options['page_type']) {
		/*
			all
			page
			post
			category
			authors
			
		*/
		
	}
	
	if(!isset($all_box_options['activation'])) $all_box_options['activation']=""; //primary activation for entire feature
	if($all_box_options['activation']=="Activate" ) {
	
		//it's okay to restrict this process in all content but pages, posts, and front pages since we wouldn't want the box showing on listing pages like categories, tags, date archives, search results, and archive listings.
		//However, if we ever want the box to show on a category page, then we need to remove this option since even manually adding a shortcode to a category PHP won't work.
		if((is_single() || is_page()) && !is_home()) {
			
			
			//////////
			/// SHORTCODE DISABLING
			//if shortcode already on page, disable it all
			if(strpos($content,$core_shortcode)===false) {
			
			//echo $all_box_options['activation'] . "; " . $all_box_options['autoinsert'] . "<br /> <br />";
			
			
			
	
	
			//////////
			/// ACTIVATION
			
			//if overall feature is activated, this adds a DIV with shortcode as long as the page isn't our homepage or blog page. This won't activate for categories or tags.
			
			
				//print_r($all_box_options);
				
				////////
				//BACKGROUND & FONT STYLING
				$background=""; $font_color="";
				
				if(!isset($all_box_options['bckgd'])) $all_box_options['bckgd']="";
				if($all_box_options['bckgd']!=="") 
					$background = "background:".$all_box_options['bckgd']."; ";
				
				if(!isset($all_box_options['font'])) $all_box_options['font']="";
				if($all_box_options['font']!=="") 
					$font = "color:".$all_box_options['font']."; ";
				
				//makes sure that something is printed to the page if nothing saved
				//this situation happens only if even the first box is deleted in the admin
				if(!isset($all_box_options['boxes'])) $all_box_options['boxes']=array('');
				if(!is_array($all_box_options['boxes'])) $all_box_options['boxes']=array('');
				
				if(is_array($all_box_options['boxes'])) {
					//print_r($all_box_options['boxes']);
					$only_one_cta=false; $nothing_possible=false;
					
					
					
					
					
					
					$box_id=""; $pos_compare=array(); $positions=array(); $kps_present="";
					$all_content=false; $key_pages_count=0;
					$few_sections=false;
					foreach($all_box_options['boxes'] as $box_id=>$box) {
						$few_sections_hdrs=false; $few_sections_all=false; 
						if(!isset($box['type'])) $box['type']="";
						if($box['type']=="") $box['type']="cta";
						//echo "<h2>".$box['type']."</h2>";
						
						// if there are few sections, then since we've already created an array of IDs and tests, we don't need to loop again.
						if($few_sections==false) {
							
							//makes sure it's an array at least to avoid issues later in the code
							if(!isset($box)) $box=array('');
							if($box=="") $box=array('');
							
							if(!isset($box['autoinsert'])) $box['autoinsert']="words";
							if($box['autoinsert']=="") $box['autoinsert']="words";
							// if box is set to shortcode, then we can deactivate all auto insert options for it
							if($box['autoinsert']!=="shortcode") {
							
								//Normally, we'd want the DOM activation outside the loop, but we don't want to load the DOM environment if the box is shortcode--only when DOM is needed
								if(!isset($html)) {
						
						
									////////
									/// PREPARE PAGE FOR INSERTION POINTS
									try {
										
										
										
										//////////
										// Initiate DOM system
										//"TRY" basically allows us to move things forward as normal if something errors out in our process (i.e. users won't know the difference)
										//To add blocks around blocks of content, we need to initial direct object modeling
										//requires PHP 5 or newer
										
										libxml_use_internal_errors(true); //indicates that you're going to handle the errors and warnings yourself and you don't want them to mess up the output of your script
										$html = new DOMDocument(null, 'UTF-8');  //makes sure that the character code matches WordPress's PHP character code since things aren't always stored the same in the DB
										//the following meta data is likely unnecessary
										@$html->loadHTML( mb_convert_encoding( $content, 'HTML-ENTITIES', 'UTF-8' ) );  //the @ indicates we don't want it to throw errors and instead just store them and keep processing
										libxml_clear_errors(); //Keeps the loadHTML part from throwing unnecessary formatting errors  //good post on this: http://stackoverflow.com/questions/1148928/disable-warnings-when-loading-non-well-formed-html-by-domdocument-php
										//is "body" even in the content filter???
										$body = $html->getElementsByTagName('body')->item(0); //all objects
										$all_content = $body->childNodes;
								
									} catch (Exception $e) {
										//if there is an error message stored by PHP, then just cancel it all and return content to avoid any obvious error to user
										return $content;
									}
								}
					
					
					

							
				
					
				
								/////////
								/// START PREPPING INDIVIDUAL BOXES
								/////////
								
								// Make sure there is even child DOMs within the BODY DOM (i.e. content is even present)
								if($all_content) {
									//echo "<h2>"."test"."</h2>";
									
									$threshold = 6; $content_count=0;
									$content_count=$all_content->length;
									if($content_count<$threshold && $content_count!==0) $few_sections=true;
									
									/*
									//if we find out that blank paragraphs are causing issues, we can activate this to skip over them when counting (but be sure to do the same below when they are actually inserted)
									foreach ($all_content as $obj) {
										//echo $obj->nodeName."; ";
										$p_cleaned=trim($obj->textContent);
										if($p_cleaned!=="") $content_count++;
									}
									*/
									
									//The following makes sure we only have one of each box type on a given page (set later in this looping section). 
									//In those cases, it shows only the first one in the list of boxes.
									if((!$only_one_cta && $box['type']=='cta') || $box['type']!=="cta") {  
										//Decided it's okay to have more than one KPS or MSG on a page. Only the CTA would look obviously duplicate. KPS and MSG are usually unique.
										//However, it's possible that two boxes would show when we don't want them to. We use negative keywords in that case.
										//|| (!$only_one_kps && $box['type']=='kps') || (!$only_one_msg && $box['type']=='msg')
										/// check if specific box is deactivated (activated is default)
										if(!isset($box['activation'])) $box['activation']="";
										if($box['activation']!=="Deactivate") {
											
											//echo "<h2 style='color:black;'>Auto-Activation Check for Box #".$box_id.":</h2>"; //testing
											
											
											//////////
											// auto activation options 
													
													
											//////////
											// EXCLUDED IDS
											// check excluded page ids if they are equal to current page
											$excludes=false;
											if(!isset($box['excludes'])) $box['excludes']=""; //avoids errors
											if(trim($box['excludes'])!=="") {
												//cycle through ids to see if the current page is in the list
												$page_ids_array=explode(",",$box['excludes']);
												//print_r($page_ids_array)."<br /> <br />";
												$page_id = get_the_id();
												foreach($page_ids_array as $exclude_id){
													if($page_id==trim($exclude_id)) $excludes=true;
												}
											}



											
											///////////
											// NEGATIVE WORDS
											// Box is deactivated automatically if any of these words appear in the title
											// if current ID is in the excludes list (i.e. $excludes=true), we don't need to do this part
											// we also dont' need to do this part if "words" isn't selected
											$keywords_array=""; $negatives="";
											if(!$excludes) {
												if($box['autoinsert']=="words") {
													if(!isset($box['negatives'])) $box['negatives']=""; //avoids errors
													elseif(trim($box['negatives'])=="") $box['negatives']=""; //avoids errors
													//cycle through keywords to see which ones are in the title
													if($box['negatives']!=="") {
														$keywords_array=explode(",",$box['negatives']); //testing
														//print_r($keywords_array)."<br /> <br />";
														if(!isset($page_title)) $page_title = strtolower(get_the_title());
														//echo "<h2>TestIt: ".$GLOBALS['trigger_words']."</h2>"; //testing
														//echo "<h2 style='color:black;'>Negatives Check for Box #".$box_id.":</h2>"; //testing
														foreach($keywords_array as $keyword) {
															if(strpos($page_title,strtolower(trim($keyword)))==true) {
																$negatives="yes";
																//echo "<h3 style='color:black;'>Found: ".$keyword."</h3>"; //testing
															}
															//else {
																//echo "<h3 style='color:black;'>Not Found: ".$keyword."</h3>"; //testing
															//}
														}
													}
													//if($negatives=="") {
														//if triggers are blank at this stage, it's likely because trigger words aren't activated
														//as a result, to make a box still show when trigger words aren't activated and keep things simple, we just make trigger=yes
														//echo "<h3 style='color:black;'>No negatives found</h3>"; //testing
													//}
													//else echo "<h3 style='color:black;'>Box won't be activated. Negatives found.</h3>"; //testing
												}
											}
											
											
											
											
											
											
											///////////
											// TRIGGER WORDS
											// if page ID isn't in list of excludes and a negative keyword isn't in a title, then see if a trigger word is in the title
											// we also dont' need to do this part if "words" isn't selected
											$keywords_array=""; $triggers="";
											if($negatives!=="yes" || !$excludes) {
												if($box['autoinsert']=="words") {
													if(!isset($box['triggers'])) $box['triggers']=$GLOBALS['trigger_words']; //avoids errors
														elseif(trim($box['triggers'])=="") $box['triggers']=$GLOBALS['trigger_words']; //avoids errors
													//cycle through keywords to see which ones are in the title
													if($box['triggers']!=="") {
														$keywords_array=explode(",",$box['triggers']); //testing
														//print_r($keywords_array)."<br /> <br />";
														if(!isset($page_title)) $page_title = strtolower(get_the_title());
														//echo "<h2>TestIt: ".$GLOBALS['trigger_words']."</h2>"; //testing
														foreach($keywords_array as $keyword) {
															if(strpos($page_title,strtolower(trim($keyword)))==true) {
																$triggers="yes";
																//echo "<h3 style='color:black;'>Found: ".$keyword."</h3>"; //testing
															}
															//else {
																//echo "<h3 style='color:black;'>Not Found: ".$keyword."</h3>"; //testing
															//}
														}
													}
													//Cases by this stage, one or more of the following are true: 
													//   Trigger words are blank
															// Causes: 
															//   Trigger words were activated but no words were found in title
													//    Trigger words =yes
															// Causes:
															//   Because trigger words are activated and at least one word was found
													
													if($triggers=="") {
														//if triggers are blank at this stage, it's because no triggers were found
														//we have to set a value outside the loop to at least verify the loop was conducted
														//even if word were activated and triggers are blank, we want to make sure the box is deactivated since we know the intention was for words to trigger the box
														$triggers="no";
														//echo "<h3 style='color:black;'>Box won't be activated</h3>"; //testing
													}
													//else echo "<h3 style='color:black;'>Box WILL be activated</h3>"; //testing
													// This "else" is true since we only define the variable if something is found and trigger words option is activated
												}
												else $triggers=="yes"; 
												//we need to define triggers at this point since we need all boxes activated by this point and we use this variable to make sure that happens if "words" is NOT selected as a trigger option
											}
											
											
											
											
											
											
											
											//if it's a key pages box, make sure there is at least one URL--deactivate otherwise
											$kps_exist=false;
											if($box['type']=="kps") {
												if(!isset($box['key_pages'][0]['url'])) $box['key_pages'][0]['url']=""; //makes sure there is at least one in the mix to avoid errors
												
												//Since we display form fields upfront, people may not delete blank ones or they may be in the middle of thinking about it
												//Make sure URLs are present before deciding whether to include the box in the ID rray for the shortcode
												$key_pages_count=0;
												foreach($box['key_pages'] as $keypage) {
													if(trim($keypage['url'])!=="") {
														if(!isset($domain)) $domain = get_bloginfo( "url" );
														//echo "<h2>".$domain."</h2>";
														if(!isset($permalink)) $permalink = the_slug(false);
														$keypage['url']=str_replace($domain,"",$keypage['url']);
														if($keypage['url']!=="/".$permalink && $keypage['url']!=="/".$permalink."/") 
															$key_pages_count++; //for next section that defines the CSS style
													}
												}
												if($key_pages_count>0) $kps_exist==true;
												else $kps_exist==false;
												//echo "<h2>".$key_pages_count."</h2>"; //testing
											}	
											
											
											//////////
											/// DETERMINE POSITIONS AND LIMIT DUPLICATION
											
											//all checks have been made by this point
											//and if it's okay to activate the box, begin the build
											if(($kps_exist==true || $triggers=="yes") && ($negatives!=="yes" && $excludes==false) ) {
												
												/*
												/// POSITIONING:
													If the page is short, then we need to combine them, no matter how many are supposed to show.
													The only way to make that happen with the shortcode is to pass a "all" type. The shortcode then will control combining them all into one box.
													When we do this, then we just plop the code 75% down the page
													Otherwise, we can use a specific type in the shortcode to get the appropriate box to show.
												*/
												
												
												/// SETUP DEFAULT POSITIONS
												$position=""; $sections_combo="";
												if(!isset($box['position'])) $box['position']="";
												if(!isset($box['sections'])) $box['sections']="";
												
												if($few_sections) {
													$position=".75";
												}
												else {
													if($box['position']=="") {
														if($box['type']!=="kps") $position=".35";
														else $position=".75";
													}
													else $position=$box['position'];
													
													// record that a type will be printed in the page
													if($box['type']=="cta") {
														$only_one_cta=true;
														$sections_combo = $box['sections'];
													}
													elseif($box['type']=="kps") {
														//$only_one_kps=true;
														$kps_present=$box_id;
													}
													elseif($box['type']=="msg") {
														//$only_one_msg=true;
														$sections_combo = $box['sections'];
													}
												}
												
												
												
												/////////
												// IMAGE CHECK FOR DESKTOPS
												//since a low amount of content could be a problem or if someone chooses .3 as a position for a box or less, 
												//we need to see if an image is at that location (DESKTOPS only)
												if($position<.3) {
													//$images = $html->getElementsByTagName('img');
													global $frn_mobile;
													if(!$frn_mobile) {
														$image_loc=stripos($content,"<img");
														if($image_loc<=800) $position=".4";
													}
												}

												
												//echo "<h2>type: ".$box['type']."; pos: ".$position."</h2>";
												
												/////////
												// BUILD ARRAY OF IDs FOR COMPARING POSITIONS
												array_push($pos_compare, array($position => $box_id));
												
												
											} //ends check if settings cause a deactivation
										} // ends activation check for the specific box it's looking at
									} /// ends the restriction to keep only one of each on a page
								} // ends verification that there is even DOMs within the BODY DOM
							} //Ends check if box's autoinsert setting is NOT set to shortcode only
							
						
						
						} //ends few sections check that avoids unnecessary looping
					} //ends boxes loop (foreach)
					
					
					
					
					////////
					/// BUILD ID Array for each position (to determine box design)
					////////
					
					// Concept:
					// loop through the array and build a new array of ids
					// leaves only unique position values to cycle through
					$box_id="";
					$uniq_positions=array();
					foreach ($pos_compare as $item=>$position_array) {
						foreach($position_array as $position=>$box_id) {
							//$position=$position_array['.75'];
							//echo  "<h2>pos: ".$position."; id: ".$box_id."</h2>";$position
							if (isset($uniq_positions[$position])) {
								$uniq_positions[$position] = $uniq_positions[$position].",".$box_id;
							}
							else 
								$uniq_positions[$position] = $box_id;
						}
					}
					//print_r($pos_compare);
					//print_r($uniq_positions);
					
					
					
					
					
					
					
					//////////
					/// PREPARE THE BOX(S) AND INSERT INTO PAGE
					//////////
					
					//// For each position in the array, build box(s) using the shortcode for one or more IDs
					$ids=""; $i=0;
					foreach($uniq_positions as $position=>$ids) {
						//echo  "<h2>pos: ".$position."; id: ".$ids."</h2>";
						
						$id_array=explode(",",$ids); $i=0; $keypages=""; $box_class_name="";
						$total=count($id_array);
						
						
						
						//////
						// Check for Dominance
						// If dominant is found, remove all other IDs with similar type
						// If more than one dominant in array, select only the first one
						if($total>1) {
							//if there's more then 1, we know we need to do loops and check dominance
							//Set the dominant type var if so
							foreach($id_array as $id) {
								$box = $all_box_options['boxes'][$id];
								if(!isset($box['dominant'])) $box['dominant']="";
								if($box['dominant']=="yes")
									${"dominant_".$box['type']} = $id;
							}
							
							//Gotta loop through them again
							//if dominant for the type is set, remove any other IDs of the same type from the array
							if(isset(${"dominant_".$all_box_options['boxes'][$id]['type']})) {
								foreach($id_array as $id) {
									$box = $all_box_options['boxes'][$id];
									if(!isset(${"dominant_".$box['type']})) ${"dominant_".$box['type']}="";
									//if it's not blank, then it means that one of the ids is dominant, so it's okay to remove any that aren't
									if(${"dominant_".$box['type']}!=="") {
										//if the current ID in the loop doesn't equal what's in the dominant var, then remove it.
										if(${"dominant_".$box['type']}!==$id) {
											//remove id from array and ids var
											unset($id_array[$id]);
											$ids=str_replace($id,"",$ids);
											$ids=str_replace(",,",",",$ids);
										}
									}
								}
							}
						}
						
						
						
						
						$dom_build_array = array();
						if($total==1) {
							//Set Key Page box CSS style
							foreach($id_array as $id) {
								$box = $all_box_options['boxes'][$id];
								//echo "<h2>".print_r($all_box_options['boxes'][$id]['key_pages'])."</h2>";
								
								//if there's only 1, it's simple
								if($box['type']=="msg") $box_class_name = "frn_msg_box";
								if($box['type']=="cta") $box_class_name = "frn_cta_banner";
								if($box['type']=="kps") {
									if(!isset($box['key_pages'])) $box['key_pages']="";
									if($box['key_pages']!=="") {
										$key_pages_count=0;
										foreach($box['key_pages'] as $keypage) {
											if(trim($keypage['url'])!=="") {
												//echo "<h2>".the_slug(false)."</h2>";
												if(!isset($domain)) $domain = get_bloginfo( "url" );
												//echo "<h2>".$domain."</h2>";
												if(!isset($permalink)) $permalink = the_slug(false);
												$keypage['url']=str_replace($domain,"",$keypage['url']);
												//echo "<h2>URL: ".$keypage['url']."; Permalink: /".$permalink."</h2>";
												if($keypage['url']!=="/".$permalink && $keypage['url']!=="/".$permalink."/") 
													$key_pages_count++; //for next section that defines the CSS style
											}
										}
										if($key_pages_count<3) {
											if($key_pages_count!==0) $box_class_name = "frn_links_short";
											else $box_class_name = "frn_links_box";
										}
										else $box_class_name = "frn_links_box";
									}
								}
								
								array_push($dom_build_array, array($id,$box_class_name));
								//$short_code = '['.$core_shortcode.' id='.$ids.']'; 
							}
						}
						elseif($total>1) {
							//What throws everything crazy complicated is because we are combining the CTA and kps
							//The order of the boxes could be in any order. So a KPS could be first in an array loop, but we need it to display second.
							//We could have more than one of the same box showing in the same position, but we want them to be seperate and not combined-----unless a CTA and kps is in the list.
								//the only way to do this is to create new DOMs for each---requiring an array approach.
						
						
							//loop through all IDs at this position
							//pull out the CTA and first KPS and store them seperately
							//all other IDs should be stored seperately and individually since they'll all get their own boxes
							$ctakps_loop=0; $cta_count=0; $kps_count=0; $msg_count=0;
							$all_others_stored=""; $kps_stored_1=""; $cta_stored="";
							foreach($id_array as $id) {
								$box = $all_box_options['boxes'][$id];
								if($box['type']=="cta") {$cta_stored=$id; $cta_count++;} //there's only one of these anyway
								elseif($box['type']=="kps") {
									//if it's the first kps in the list, plan to combine it with the CTA
									if($kps_count<1) {
										$kps_stored_1=$id;
										//echo "<h2>cta_stored: ".$cta_stored."; kps_stored_1: ".$kps_stored_1."</h2>";
									}
									else {
										if($all_others_stored=="") $all_others_stored.=$id;
										else $all_others_stored.=",".$id; //otherwise, create an array of the kps
									}
									$kps_count++;
								}
								else {
									if($all_others_stored=="") $all_others_stored.=$id;
									else $all_others_stored.=",".$id; //otherwise, create an array of the kps
									$msg_count++;
								}
								$ctakps_loop++;
							}
							
							//Make sure CTA shows first in the new dom build array
							if($cta_stored!=="") {
								//there's only one cta in the list (limited at the top of the larger loop of boxes)
								//CSS is the same even if kps are combined with it
								//Since kps can be included without changing much, we can plan for it to be blank
								//This needs to be the first in the new dom_build_array
								if($kps_stored_1!=="") $kps_stored_1=",".$kps_stored_1;
								$box_class_name = "frn_cta_banner";
								array_push($dom_build_array, array($cta_stored.$kps_stored_1,$box_class_name));
								//$short_code = '['.$core_shortcode.' id="'.$cta_stored.",".$kps_stored_1.'"]'; 
							}
							elseif($kps_stored_1!="") {
								//If CTA is not one of the boxes at this position and the first KPS box was stored, then add the KPS to the front of the "other" ids
								if(trim($all_others_stored)!=="") $all_others_stored = $kps_stored_1.",".$all_others_stored ;
								else $all_others_stored = $kps_stored_1;
							}
							
							
							
							//if there are any other IDs, add them to the array. 
							//None will be a CTA box.
							//echo "<h2>All Others: ".$all_others_stored."</h2>";
							if(trim($all_others_stored)!=="") {
								$all_others_stored=explode(",",$all_others_stored);
								//print_r($all_others_stored);
								foreach($all_others_stored as $other_id) {
									//echo "<h2>Other ID: ".$other_id."</h2>";
									$box = $all_box_options['boxes'][$other_id];
									//Determine class names
									if($box['type']=="kps") {
										if(!isset($box['key_pages'])) $box['key_pages']="";
										if($box['key_pages']!=="") {
											$key_pages_count=0;
											foreach($box['key_pages'] as $keypage) {
												if(trim($keypage['url'])!=="") {
													//echo "<h2>".the_slug(false)."</h2>";
													if(!isset($domain)) $domain = get_bloginfo( "url" );
													//echo "<h2>".$domain."</h2>";
													if(!isset($permalink)) $permalink = the_slug(false);
													$keypage['url']=str_replace($domain,"",$keypage['url']);
													if($keypage['url']!=="/".$permalink && $keypage['url']!=="/".$permalink."/") 
														$key_pages_count++; //for next section that defines the CSS style
												}
											}
											if($key_pages_count<3) {
												if($key_pages_count!==0) $box_class_name = "frn_links_short";
												else $box_class_name = "frn_msg_box";
											}
											else $box_class_name = "frn_links_box";
										}
									}
									else $box_class_name = "frn_msg_box";
									//echo "<h2>ID: ".$other_id."; Style: ".$box_class_name."</h2>";
									array_push($dom_build_array, array($other_id,$box_class_name));
								}
							}
						}
						else echo "
						
						<!-- NOTICE: No IDs are in id_array for position #".$position." -->
						
						"; //This only happens when $total=0 -- which shouldn't ever happen
						
						
						
						
						
						/////
						// BUILD DOM ARRAY
						//print_r($dom_build_array);
						$doms_count=0;
						foreach($dom_build_array as $dom_items) {
							//print_r($dom_items);
							$short_code = '['.$core_shortcode.' id="'.$dom_items[0].'"]';
							//////
							/// Prepare box container
							${"box_dom_".$doms_count} = $html->createElement('div',$short_code); 
							if(${"box_dom_".$doms_count}) {
								$box_class = $html->createAttribute('class');
								$box_class->value = $dom_items[1]." frn_box_".str_replace(",","",$dom_items[0]);
								${"box_dom_".$doms_count}->appendChild($box_class);
								if($background!=="" or $font_color!=="") {
									$box_style = $html->createAttribute('style');
									$box_style->value = $background.$font;
									${"box_dom_".$doms_count}->appendChild($box_style);
								}
							}
							//echo "<h2>Box Created: "."box_dom_".$doms_count."</h2>";
							$doms_count++;
						}
						
						
						
						
						
						////////
						/// PLACE DOMS ON THE PAGE AT THE $position
						//if there is at least one dom created
						if($box_dom_0) {
							
							// Make sure there is even content on the page
							// Don't insert DOMs if not
							if($content_count > 0) {
								
								
								
								/////
								// CALCULATE INTENDED POSITION ON THE PAGE
								$insert_before = round($content_count * $position); //intval() //drops decimals and just sticks with the integer
								//echo "total: ".$all_content->length."; original: ".$insert_beforep;
								//echo  "<h2>content: ".$content_count."; position: ".$position."; id=".$ids."</h2>";
								
								
								
								
								////////
								/// SET SECTION POSITION DETERMINANT (HEADERS OR ANYWHERE)
								// Determine if settings determine if we specifically want to use headers to position a box or not
								// PROBLEM: If two boxes are showing at the same position, but one wants to use headers and the other any section, the first ID in the id_array will be used to determine that.
								$section_pos="";
								foreach($id_array as $id) {
									if($section_pos=="") {
										if(!isset($all_box_options['boxes'][$id]['sections'])) $all_box_options['boxes'][$id]['sections']="";
										if($all_box_options['boxes'][$id]['sections']=="") {
											if(!isset($all_box_options['boxes'][$id]['type'])) $all_box_options['boxes'][$id]['type']="cta";
											if($all_box_options['boxes'][$id]['type']=="kps") 
												$all_box_options['boxes'][$id]['sections']="Headers";
											else
												$all_box_options['boxes'][$id]['sections']="Anywhere";
										}
										//Official var set for section type
										$section_pos = $all_box_options['boxes'][$id]['sections'];
									}
								}
								
								
								
								
								
								///////////////
								/// PLACE INTO THE PAGE
								$prev_node=""; $ip=0;
								foreach ($all_content as $obj) {
									$ip++; 
									
									//FOR TESTING: so we don't see something for all sections on the page
									//if($ip==2) { 
									//	print_r($obj);  
									//	echo "<div>".$obj.innerText."</div>";
									//}
									
									//the following may be valuable, but not sure. If there are blank p tags, it ads extra spacing. So this checks if it's blank. We can then just increment by 1 if we don't want this to be considered.
									//but so far, it doesn't seem necessary. Keeping in case we determine otherwise.
									//$p_cleaned=trim($obj->textContent);
									
									//RULES:
									//if next is UL, insert box after that by just adding 1 to the insert_before position
									//if this one is a header, then insert before it to keep things simple
									
									//FOR TESTING:
									//if($obj && $obj->nextSibling) 
									//	echo "<h2>Current Node (#".$ip."): ".$obj->nodeName."; Next Node: ".$obj->nextSibling->nodeName."</h2>";
									//echo "<h2>".$insert_before."</h2>";
									
									
									if($ip==$insert_before) {
										if($obj->nodeName=="h2") {
											//if header, just insert the box(s) before it
											$dom_loop=0;
											do {
												$obj->parentNode->insertBefore(${"box_dom_".$dom_loop}, $obj);
												$dom_loop++;
											}
											while($dom_loop<$doms_count);
											
											//echo "<h2>Current Node: ".$obj->nodeName."; Next Node: ".$obj->nextSibling->nodeName."</h2>";  //testing
										}
										elseif($obj->nextSibling) {
											// if not a header, then see if there is a nextSibling
											// if so, see if the nextSibline is a text node or UL. 
											// if it is, then skip this one by adding 1 to the insert_before
											// if it is not, then just insert the box(s) after it
											if($all_box_options['boxes'][$id]['sections']=="Headers") {
												$siblings=$ip+1;
												//echo  "<h2>Sibling nodeName: ".$obj->nextSibling->nodeName." at object #".$siblings."</h2>";  //testing
												$prev_sibling="";
												if($sibling=$obj->nextSibling) {
													//if there is a next sibling, see if it's an h2--if so, insert before
													if($sibling->nodeName=="h2") {
														$dom_loop=0;
														do {
															$obj->parentNode->insertBefore(${"box_dom_".$dom_loop}, $sibling);
															$dom_loop++;
														}
														while($dom_loop<$doms_count);
														//Old: $obj->parentNode->insertBefore($box_dom, $sibling);
														//echo  "<h2>Initial position: ".$insert_before.". Sibling Found for ".$short_code." at next position #".$siblings." with nodeName ".$sibling->nodeName."</h2>";  //testing
													}
													else {
														//otherwise start cycling to the end until there is an h2
														while($sibling==true && $sibling->nodeName!=="h2") {
															$siblings++;
															$prev_sibling=$sibling;
															if($sibling=$sibling->nextSibling) {
																//echo  "<h2>Sibling nodeName: ".$sibling->nodeName." at object #".$siblings."</h2>";  //testing
																if($sibling->nodeName=="h2") {
																	$dom_loop=0;
																	do {
																		$obj->parentNode->insertBefore(${"box_dom_".$dom_loop}, $sibling);
																		$dom_loop++;
																	}
																	while($dom_loop<$doms_count);
																	//echo  "<h2>Initial position: ".$insert_before.". Sibling Found for ".$short_code." at next position #".$siblings." with nodeName ".$sibling->nodeName."</h2>";  //testing
																}
															}
														}
													}
												}
												//But if it reaches the end of the page, then just insert the box(s) at the end of the page
												if(!is_object($sibling) && $prev_sibling!="") {
													//Old: $obj->parentNode->insertBefore($box_dom, $prev_sibling->nextSibling);
													$dom_loop=0;
													do {
														$obj->parentNode->insertBefore(${"box_dom_".$dom_loop}, $prev_sibling->nextSibling);
														$dom_loop++;
													}
													while($dom_loop<$doms_count);
												}
											}
											elseif($obj->nextSibling->nodeName=="#text" || $obj->nextSibling->nodeName=="ul" || $obj->previousSibling->nodeName=="h2") $insert_before++;
											else {
												$dom_loop=0;
												do {
													//echo "<h2>Suppose to print box</h2>";
													$obj->parentNode->insertBefore(${"box_dom_".$dom_loop}, $obj->nextSibling);
													$dom_loop++;
												}
												while($dom_loop<$doms_count);
												//$obj->parentNode->insertBefore($box_dom, $obj->nextSibling);
											}
										}
										//echo "; new: ".$insert_before;  //testing
									}
								}
							} // end of requirement that there be at least one section on the page
						} // Ends verification that the box was created successfully
				
						$i++;
						
					}  //end of positions array foreach
				
				
				
				
					////////
					/// PRINT CHANGES TO THE PAGE
					////////
					
					// if a box dom was never created in one of the loops, then just return content
					if(!isset($box_dom_0)) 
						return $content;
					elseif($html) {
						//otherwise, if the HTML DOM was initiated, it's okay to save it to the page now
						//This "saveHTML" option does the same thing as returning a variable
						//However, this approach to the content includes HTML,BODY,HEAD,and META tags with it as if it were a full HTML page. The Preg_replace removes those things. 
						//There may be a faster way, however. I think the regular expression method is pretty processor intense.
						return preg_replace('~<(?:!DOCTYPE|/?(?:html|body|head|meta))[^>]*>\s*~i', '', $html->saveHTML()); // If DOM environment loaded
						$box_dom=""; //releases memory
						$html=""; //releases memory
					} //Otherwise, if neither DOM or HTML was activated, just return the page without any changes
					else 
						return $content; //if no DOM environment, then just return the page
			
			
				} // ends check if it's an array (just a fallback for unexpected issues)
				else return $content;
				
			}  /// end of requirement that it should be a "post" or "page" type of page
			else return $content;
			
		} //end check if shortcode already in content
		else return $content;
		
	} //end activation check
	else return $content;
	
} //end of function
}
add_filter('the_content','frn_incontent_boxes');




/////////
//// Personalization Boxes_Shortcode
if(!function_exists('frn_personal_function')) {
function frn_personal_function($atts) {
	extract( shortcode_atts( array(
		'id'	=>	'',
	), $atts, 'frn_boxes' ) );
	
	//make sure this shortcode doesn't execute in content for listing pages
	if(!is_category() && !is_home() && !is_tag() && !is_author() && !is_date() && !is_search()) {
		
		$all_box_options = get_option($GLOBALS['personal_box_var']);
		
		//print_r($id);
		
		//since by default this is deactivated, we only need to see if it has one thing saved to know whether to go forward or not.
		if(isset($all_box_options['activation'])) {
			
			
			//print_r($all_box_options);
			
			//////
			// find the box with an id
			// the shortcode will most often be looking for a specific box
			// but sometimes we'll want two boxes close together on a short article.
			// in those cases, we need to use "ids" instead of "id" to trigger the combination of mroe than one box into one area or DIV wrap
			
			
			if(!isset($id)) $id=0;
			//turns ID into an array whether or not it has comma delimiters
			$id_array = explode(",",$id);
			$id=""; //reassigning to keep things clear
			$total = count($id_array);
			$box_html="";
			//print_r($id_array);
			
			
			///////
			/// DOMINANT BOXES
			///////
			//Sometimes more than one of the same box may show at a position. 
			//In those cases, if one is more dominant, then the system will disable the other from showing
			//The following defines a variable name by combining the box type with text
			//if more than one at a position, check if there is a dominant box defined
			if($total>1) {
				foreach($id_array as $id) {
					//if the dominant type variable isn't set yet, then see if the current looped ID is dominant. Record it if so.
					//this makes sure that only the first dominant one in the list per type is recorded. Any subsequent ones will be forgotten.
					if(!isset(${"dominant_".$all_box_options['boxes'][$id]['type']})) {
						if(!isset($all_box_options['boxes'][$id]['dominant'])) $all_box_options['boxes'][$id]['dominant']="";
						if($all_box_options['boxes'][$id]['dominant']=="yes")
							${"dominant_".$all_box_options['boxes'][$id]['type']} = $id;
					}
				}
			}
			
			
			
			//Sometimes two or more boxes are at the same position.
			//To make them both show, we put two or more IDs in the same ID variable as an array
			//the foreach will run as many times as there are IDs
			$box_errors="";
			foreach($id_array as $id) {
				
				
				//Makes sure the box exists (since a person could type in the shortcode, they may type it in wrong)
				if(isset($all_box_options['boxes'][$id])) {
					$box = $all_box_options['boxes'][$id];
					//echo "<h2>".$id."</h2>";

					
					//see if dominant type is set
					//if so, see if the current id is the dominant one. 
					//If not, set print_id to no.
					//the great thing with this setup is that print_id will always be yes unless a dominant variable is set for it's specific type
					//echo "<h2>".${"dominant_".$box['type']}."</h2>";
					if(!isset(${"dominant_".$box['type']})) ${"dominant_".$box['type']}="";
					if(${"dominant_".$box['type']}!=="") {
						if(${"dominant_".$box['type']}==$id) $print_id="yes";
						else $print_id="no";
					}
					if(!isset($print_id)) $print_id="yes";
					
					
					//If okay to process id, do so
					if($print_id=="yes") {
					
					
						// NOTES:
						///If someone deletes all boxes in settings or includes a shortcode without an id, it's possible no variables will be set. 
						// Therefore, instead of doing a main isset element to determine if it's printed to the page, we use defaults instead and isset every variable (which we'd do anyway)
						
						//We only want the box hidden if it's set to deactivate
						if(!isset($box['activation'])) $box['activation']="";
						if($box['activation']!=="Deactivate") {
							
							//echo "<h2>".$box['type']."</h2>";
							
							//default box type
							if(!isset($box['type'])) $box['type']="";
							if($box['type']=="") $box['type']="cta";
				
							
							
							
							//////////
							//// PREP MESSAGES
								//default message for CTA (blank if other)
								if($box['type']=="cta") {
									if(!isset($box['message'])) $box['message']=$GLOBALS['message_cta'];
										elseif($box['message']=="") $box['message']=$GLOBALS['message_cta'];
								}
								elseif(!isset($box['message'])) $box['message']="";
							
							
							
								///////
								//// DRUG WORD SUBSTITUTES
								if(trim($box['message'])!=="") {
									$drug_sub_pos=false; $drug_or_pos=false;
									if(strpos($box['message'],"%%drug|")!==false) $drug_sub_pos = true;
									elseif(strpos($box['message'],"%%drug%%")!==false) $drug_or_pos = true;
									
									if($drug_or_pos || $drug_sub_pos) {
								
										//////////
										/// INSERT DRUG WORD INTO MESSAGE
										//////////
										//Go through our popular drugs and see if they are in the title
										//Search plural forms first, then singular
										$drug_array = array(
											"hydrocodone",
											"vicodin",
											"xanax",
											"rivotril",
											"alcoholic",
											"alcoholism",
											"alcohol",
											"klonopin",
											"ativan",
											"clonazepam",
											"lorazepam",
											"percocet",
											"roxies",
											"alprazolam",
											"ambien",
											"suboxone",
											"valium",
											"zolpidem",
											"diazepam",
											"heroin",
											"ketamine",
											"rohypnol",
											"alcoholism",
											"opiate",
											"opium",
											"roxycodone",
											"painkiller",
											"benzos",
											"benzodiazepine",
											"benzo",
											"depressant"
										);
										
										$drug_name="";
										$page_title = strtolower(get_the_title());
										$blog_name = strtolower(get_bloginfo( 'name' ));
										$total_drugs = count($drug_array);
										
										//look in page title first
										$i=0;
										while($drug_name=="" && $i<$total_drugs) {
											//this stops once a drug is found
											if(strpos($page_title,$drug_array[$i])!==false) {
												$drug_name=$drug_array[$i];
												//if($i==0) echo "<h2>Drug: ".$drug_array[$i]."; ".strpos($page_title,$drug_array[$i])."</h2>";
											}
											$i++;
										}
										
										
										if(trim($drug_name)=="") {
											$i=0;
											while($drug_name=="" && $i<$total_drugs) {
												//this stops once a drug is found
												if(strpos($blog_name,$drug_array[$i])!==false) $drug_name=$drug_array[$i];
												$i++;
											}
										}
									
									
									
										//make sure it's plural even if singular version found
										//Don't add these plurals above.
										//These have to be here a second time
										if($drug_name!="") {
											/// order: array(singular,plural)
											$switch_array = array(
												array("alcoholic","alcohol"),
												array("alcoholism","alcohol")
											);
											foreach($switch_array as $switcheroo){
												//$drug_name=str_replace($switcheroo[0],$switcheroo[1],$drug_name);
												$drug_name=preg_replace('/^'.$switcheroo[0].'$/', $switcheroo[1], $drug_name); 
											}
										}
									
										//set site name as drug if not found in title
										$or=""; if($drug_or_pos) $or=" or ";
										if($drug_name!=="") $drug_name = $or.$drug_name." addiction";
										if($drug_sub_pos) {
											if($drug_name!=="") $box['message']=preg_replace('/%%drug\|(.*)%%/', $drug_name, $box['message']);  //replace all with drug name
											else $box['message']=preg_replace('/%%drug\|(.*)%%/', '$1', $box['message']); //replace with drug name even if blank so that code is removed
										}
										else $box['message']=str_replace(' %%drug%%',$drug_name,$box['message']); //for regular use, replace with drug name. Or will be part of it if included.
										
									} //Ends drug word option
								} //Ends requirement that a message is present
						
						
						
							
							/////////
							//Contact buttons for CTAs
							$contact_buttons = "";
							if($box['type']=="cta") {
								if(!isset($box['chat_id'])) $box['chat_id']="";
									if(trim($box['chat_id'])!=="") $box['chat_id'] = "id=\"".$box['chat_id']."\"";
								if(!isset($box['email_image'])) $box['email_image']="";
									if(trim($box['email_image'])!=="") $box['email_image']= "url=\"".$box['email_image']."\"";
								$contact_buttons = '
							<div class="frn_contact_options">'.
								do_shortcode('[lhn_inpage button="chat" '.$box['chat_id'].']').
								'<div class="hide-for-small-only">'.do_shortcode('[frn_phone ga_phone_location="Phone Clicks in CTA"]').'</div>'.
								do_shortcode('[lhn_inpage button="email" '.$box['email_image'].']').'
							</div>
							';
							}
							
					
							

							////////
							//key pages links section
							$key_posts_box="";
							if($box['type']=="kps") {
								
								//Setting blank variable is likely unnecessary at this point because the shortcode for the keypages box wouldn't even be included if there wasn't at least one URL in the mix.
								//But just to make sure bases are covered (and it was already written) and since it's lightweight, I kept the code here.
								if(!isset($box['key_pages'])) $box['key_pages']="";
								if($box['key_pages']!=="") {
									$key_pages_count=0;
									foreach($box['key_pages'] as $keypage) {
										if(trim($keypage['url'])!=="") {
											//echo "<h2>".the_slug(false)."</h2>";
											if(!isset($domain)) $domain = get_bloginfo( "url" );
											//echo "<h2>".$domain."</h2>";
											if(!isset($permalink)) $permalink = the_slug(false);
											$keypage['url']=str_replace($domain,"",$keypage['url']);
											if($keypage['url']!=="/".$permalink && $keypage['url']!=="/".$permalink."/") 
												$key_pages_count++; //for next section that defines the CSS style
										}
									}
								}
								
								// && trim($box['key_pages'][0]['url'])!==""
								//echo "<h2>URL: ".$box['key_pages'][0]['url']."</h2>";
								/// make sure kps is the type and that at least the first item in the array has a url
								 
								if($box['type']=="kps" && trim($box['key_pages'][0]['url'])!=="") {
								
									$key_posts_html=""; $kps=1;
									foreach($box['key_pages'] as $keypage) {
										
										if(!isset($keypage['url'])) $keypage['url']="";
										$url = trim($keypage['url']);
										
										if($url!=="" && ($url!=="/".$permalink && $url!=="/".$permalink."/")) {
											if(!isset($keypage['anchor'])) $keypage['anchor']="";
											if(!isset($keypage['hover_text'])) $keypage['hover_text']="";
											
											//set values to reduce error log
											$hover_text = trim($keypage['hover_text']);
											$anchor = trim($keypage['anchor']);
											if($anchor=="") $anchor = get_the_title( url_to_postid( $url ) );

											//If URL is in the system, then process the link
											//Removing this to increase page load but can put back in if we have a problem with broken links
											//if(frn_check_url($url)) {
											$font_style="";
											if(!isset($all_box_options['font'])) $all_box_options['font']="";
											if($all_box_options['font']!=="") {
												$font_style = " style=\"".$all_box_options['font'].";\" ";
											}
											
											if($key_pages_count<2) $see_article = "See Article: ";
											else $see_article = "";
											
											$key_posts_html .= '
									<li>'.$see_article.'<a href="'.$url.'" title="'.$hover_text.'"'.$font_style.' onClick="if(typeof ga===\'function\') ga(\'send\', \'event\', \'Context Key Pages\', \''.$anchor.'\',\''.$url.'\');" >'.$anchor."</a></li>
									";
											//echo "<h2>".$key_posts_html."</h2>"; //testing
										
											//}
											//else {
											//	$key_posts_html .= "<!-- ".$url." is not a page on this site -->"; 
											//	$url_fail_notice="
										//<!-- Key Page URL #".$kps." (".$url.") is not a page on this site -->
										//"; 
										//	}
										}
										$kps++;
									} //Ends foreach key page
									
									$start=""; $end=""; $title="";
									if($total==1) {
										if($key_pages_count<3) $title = "
								<h4>Recommended for You:</h4>";
										else $title = "
								<h4>Things to Consider</h4>";
										//$start="<div class=\"frn_links_box\">";
										//$end="</div>";
									}
											
									//If there is at least 1 post in list, add the wrapping UL
									if($key_pages_count>0) {
										$key_posts_box = $title.'
								<ul>'.$key_posts_html."
								</ul>
								";
									}
									else $key_posts_box = "<h3>We value you and want to help.</h3>"; //.$url_fail_notice  //backup but likely won't be used
								
								}
							}					//End of requirement that type is kps and there is something saved for key_pages
							
							$box_html.=$box['message'].$contact_buttons.$key_posts_box.$box_errors;
							
						} //End check if individual box is deactivated
						//else return;
					} //ends verification that it is the dominant box
				} //ends check that the box even exists in the DB
				else {
					$box_errors.="
								
								<!-- Box id#".$id." does not exist in the database -->
								";
				}
			} //ends foreach that loops through the boxes to find the one with the ID
			
			///////
			// BOX CONSTRUCTION
			return $box_html;
					
					
		} //Check if the activation option is even set
		else return;
	} //ends page type check so it doesn't run on listing pages
	else return;
} //End of function
}


/////////
// Personalization URL_Check
if(!function_exists('frn_check_url')) {
function frn_check_url($url) {
	//Checks if a URL is actually on the site---avoids any broken links
	//Typically used in admin, but also a safety check on page--deactivates a link if it doesn't exist

	$postid = "";
	$category = false;
	$title = false;
	
	//make sure page is on site
	if(strpos(trim($url), "/")==0) $WP_permalink = substr($url, 1, strlen($url)-1); //removes first slash 
	if(substr($url, -1, 1)=="/") $WP_permalink = substr($WP_permalink, 0, strlen($WP_permalink)-1); //removes last slash
	$WP_permalink = str_replace("category/","",$WP_permalink);
	$postid = url_to_postid( $WP_permalink );
	//echo "<h2>".$postid."</h2>";
	
	if($postid!==0) {
		//return true;
		$title = get_the_title($postid);
	}
	else {
		$category = get_category_by_path($WP_permalink);
		if($category) {
			//echo $category->name . "<br />";
			//return true; 
			$title = $category->name; 
		}
	}
	
	return $title;
}
}
if(!function_exists('the_slug')) {
function the_slug($echo=true){
  $slug = basename(get_permalink());
  do_action('before_slug', $slug);
  $slug = apply_filters('slug_filter', $slug);
  if( $echo ) echo $slug;
  do_action('after_slug', $slug);
  return $slug;
}
}