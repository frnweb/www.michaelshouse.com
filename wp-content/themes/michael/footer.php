<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
 <!-- Speak Section -->
        <section class="speak">
            <h4 class="text-center"><p>Speak with an Admissions Coordinator <span class="bold number"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Admissions Coordinator" ]'); ?><?php //echo ot_get_option("speak_with_content"); ?></span></p></h4>
        </section>
    </div>
    <!-- Main Content Ends -->
  <!-- Footer -->
    <footer>
        <div class="row">
            <!--Footer Top Section -->
            <div class="footer-top clearfix">
                <div class="small-12 large-4 columns  footer-top-left">
                    <div class="footer-box">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="michael's house"> <img src="<?php echo ot_get_option("header_logo"); ?>" alt="footer-logo"> </a>
                        <!-- Social Icons -->
                        <?php

                        //////
                        // APPLY NEW WINDOW TARGET FOR DESKTOP ONLY
                        global $frn_mobile; $target=' target="_blank"';
                        if(isset($frn_mobile)) {
                            if($frn_mobile) $target="";
                        }
                        ?>
                        <ul class="social">
                            <li><a href="<?php echo ot_get_option("twitter_link"); ?>"<?=$target;?> ><i class="fa fa-twitter"></i></a></li>
                            <li><a href="<?php echo ot_get_option("facebook_link"); ?>"<?=$target;?> ><i class="fa fa-facebook"></i></a></li>
                            <li><a href="<?php echo ot_get_option("linkedin_link"); ?>"<?=$target;?> ><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="<?php echo ot_get_option("youtube"); ?>"<?=$target;?> ><i class="fa fa-youtube-play"></i></a></li>
                        </ul>
                        <!-- Address -->
                        <p class="big"> <?php echo ot_get_option("address"); ?></p>
                        <p class="big small-line"><?php echo ot_get_option("proud_member_text"); ?> </p>
                    </div>
                </div>
                <!--Footer Top Right -->
                <div class="small-12 medium-12 large-8 columns float-right footer-top-right">
                    <div class="row">
                        <!-- Footer Box 1 -->
                        <div class="columns medium-3">
                            <div class="footer-box">
                                <small>Explore our site</small>
                                <?php wp_nav_menu(array('container' => '', 'menu' => 'Explore our site', 'menu_class' => '')); ?>
                            </div>
                        </div>
                        <!-- Footer Box 2 -->
                        <div class="columns medium-3">
                            <div class="footer-box">
                                <small>Research &amp; Learn</small>
                                <?php wp_nav_menu(array('container' => '', 'menu' => 'Research & Learn', 'menu_class' => '')); ?>
                            </div>
                        </div>
                        <!-- Footer Box 3 -->
                        <div class="columns medium-3">
                            <div class="footer-box">
                                <small>Get In Touch</small>
                                <?php wp_nav_menu(array('container' => '', 'menu' => 'Get In Touch', 'menu_class' => '')); ?>
                            </div>
                            <div class="footer-box">
                                <small>Heal With Us</small>
                               <?php wp_nav_menu(array('container' => '', 'menu' => 'Heal With Us', 'menu_class' => '')); ?>
                            </div>
                        </div>
                        <!-- Footer Box 4 -->
                        <div class="columns medium-3">
                            <div class="footer-box">
                                <small>MEET OUR NETWORK</small>
                               <?php wp_nav_menu(array('container' => '', 'menu' => 'Meet Our network', 'menu_class' => '')); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row footer-accreditations">
            <div class="columns small-12 medium-4">
                <script src="https://static.legitscript.com/seals/786341.js"></script>
            </div>
            <div class="columns small-12 medium-4">
                <?php if(ot_get_option("bbb_logo")):?>
                <a href="<?php echo ot_get_option("bbb_link"); ?>"<?=$target;?> class="mt-25" ><img src="<?php echo ot_get_option("bbb_logo"); ?>" alt="Michael's House BBB Accredited Business Profile"></a>
                <?php endif;?>
            </div>
            <div class="columns small-12 medium-4">
                <img class="naatp-logo" alt="NAATP "src="<?php bloginfo('stylesheet_directory'); ?>/images/accreditations/naatp-stacked-white.svg">
            </div>
        </div><!-- /.row -->
        <!-- Footer Bottom -->
        <div> <?php echo do_shortcode('[frn_footer]'); ?><?php //echo ot_get_option("copyright"); ?></div>
    </footer>
    <!-- Popup Video -->
    <div class="reveal large" id="video" data-reset-on-close="true" data-reveal data-close-on-click="true">
        <div class="flex-video widescreen">
            <?php echo ot_get_option("iframe_code"); ?>
        </div>
    </div>
    

    <!-- VOB Form Pop Up -->

    <?php 
	if ( is_page( array( 'finding-freedom-lgbtq-symposium', 'verify-your-benefits', 'vob-thank-you' ) )  ) { 
    	//do nothing on these pages, otherwise show the VOB Popup
	}
	else {
		get_template_part( 'parts/content', 'vob-popup' ); //showing the VOB form popup
	}
	?> 

    <!-- VOB Form Pop Up Ends -->
 
   	   
       <!-- Stylesheet -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/foundation.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/slick.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/app.css?ver=1.1">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style-responsive.css?v=1.02">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/frn.css?ver=1.1">


    <!-- Javascript
    this is the old pyxl script
    <script src="https://use.typekit.net/lwz0pjg.js"></script>
    ben wrights old script <script src="https://use.typekit.net/mzm7wxg.js"></script>-->
    <script src="https://use.typekit.net/zrb6jyz.js"></script>
    

    <script>
    try {
        Typekit.load({
            async: true
        });
    } catch (e) {}
    </script>
	
    <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery.js"></script>
	<script>jQuery.noConflict(true);</script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/what-input.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/foundation.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/foundation.orbit.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/foundation.magellan.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/foundation.reveal.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/foundation.tabs.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/vob-conditional-form.js"></script><!-- VOB CONDITIONAL LOGIC -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/slick.min.js"></script>
 	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.cookie.js"></script><!-- added this from old MH site for cookie -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/app.js?v=1.01" ></script>
	<script>

jQuery(document).ready(function() {
	
	jQuery('.dropdown').each(function(){
		
		if(jQuery(this).find('.submenu_class').length==0)
		{
			        jQuery(this).children('ul').remove();
		}
		});
});
</script>
	<?php wp_footer(); ?>

<!--<script src="//cdn.trackduck.com/toolbar/prod/td.js" async data-trackduck-id="589e1d22026b48e6591bd676"></script>
-->
  <!-- this is the mobile popover from the old site -->
   <?php global $frn_mobile; ?>
   
    <?php /* if($frn_mobile == "Smartphone") {?>
   <div id="mobile_overlay" class="">
    <div id="mobileCTA">
		<div id="close_btn"><i class="fa fa-times" aria-hidden="true"></i></div>
        <h3 class="text-center">Speak to a Professional</h3>
        <p class="text-center orange phone-cta"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Mobile Lightbox"]'); ?></p>        
        <p>We care about your unique situation and needs. At Michael's House, our goal is to provide lifetime recovery solutions.</p>
    </div><!-- end mobile CTA -->
</div><!-- end mobile_overlay -->
    
    <script type="text/javascript">
   	 jQuery(document).ready(function() {
            if (jQuery.cookie('modal_shown') == null) {
                jQuery.cookie('modal_shown', 'yes', { expires: 7, path: '/' });
                setTimeout(function(){
                    jQuery('#mobile_overlay').delay(5000).addClass('visible');
					jQuery('#close_btn').click(function(){
					jQuery('#mobile_overlay').removeClass('visible');
						  });
            }, 4000);
        }
    });
    </script>
<?php } */?><!-- end frn function on for mobile detection on popover -->

<script type="text/javascript">
 jQuery(document).ready(function() {
        if (jQuery.cookie('vob_popup') == null) {
            jQuery.cookie('vob_popup', 'yes', { expires: 1, path: '/' });
            setTimeout(function(){
                jQuery('#vob-popup').foundation('open');
        }, 10000);
    }
});
</script>

</body>
</html>
