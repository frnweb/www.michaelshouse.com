<?php
/**
 * Template Name: Page With Sidebar
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<?php while ( have_posts() ) : the_post();
$feature_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );  
if( ! $feature_image ) {
    $feature_image = ot_get_option("default_header_image");
}
?>
 <!-- Main Content -->
<div class="main-content">
    <!-- Innerpage Banner -->
    <section class="banner banner-innerpage" style="background-image:url(<?php echo $feature_image;?>);">
        <!-- Innerpage Banner Caption -->
        <div class="caption">
            <h1><?php the_title();?></h1>
        </div>
    </section>
    <!-- Banner Ends -->
    <!-- Content Section -->
    <div class="row">
    <div class="medium-9 columns">
       <!-- Content Section -->
        <div class="row sec">
            <div class="small-12 small-centered large-centered columns">
            <?php the_content();?>
            </div>
        </div>
    <?php endwhile; ?>

         <!-- Content Section -->
        <?php include('flexible-content.php'); ?>
    </div>
    <!-- Sidebar Section -->
    <aside class="medium-3 columns">
       
        <?php dynamic_sidebar('sidebar-1');?>
    </aside>
</div>
   

<?php
get_footer();
