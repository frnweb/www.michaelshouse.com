<?php while(the_flexible_field("content")): ?>
		<?php if(get_row_layout() == "text"): ?>
        <div class="row sec">
            <div class="small-12 medium-10 small-centered large-centered columns">
               
                   <?php the_sub_field("block_content"); ?>
               
            </div>
        </div>
		<?php elseif(get_row_layout() == "image_content"): ?>
        <div class="row sec" data-equalizer>
            <div class=" mb-0">
                <div class="medium-6 columns out-vc" data-equalizer-watch>
                    <div class="featured-box-img in-vc">
                        <?php if(get_sub_field('image')) : 
                        $img = get_sub_field('image');
                        $img_id = get_attachment_id($img);
                        $img_meta = wp_get_attachment_metadata($img_id, true);
                        $opt_image = wp_image_add_srcset_and_sizes($img, $img_meta, $img_id);
                        ?>
                            <figure class="">
                                <img src="<?php echo $opt_image; ?>" alt="section-img">
                            </figure>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="medium-6 columns out-vc" data-equalizer-watch>
                    <div class="featured-box-text wo-bdr in-vc">
                        <?php the_sub_field("description"); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php elseif(get_row_layout() == "image_content_right"): ?>
        <div class="row sec" data-equalizer>
            <div class=" mb-0">
                <div class="medium-6 columns out-vc" data-equalizer-watch>
                    <div class="featured-box-text wo-bdr in-vc">
                        <?php the_sub_field("description"); ?>
                    </div>
                </div>
                <div class="medium-6 columns out-vc" data-equalizer-watch>
                    <div class="featured-box-img in-vc">
                        <?php if(get_sub_field('image')) : 
                        $img = get_sub_field('image');
                        $img_id = get_attachment_id($img);
                        $img_meta = wp_get_attachment_metadata($img_id, true);
                        $opt_image = wp_image_add_srcset_and_sizes($img, $img_meta, $img_id);
                        ?>
                            <figure class="">
                                <img src="<?php echo $opt_image; ?>" alt="section-img">
                            </figure>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
		<?php elseif(get_row_layout() == "two_columns_content"): ?>
         <!-- Row with 2 Images -->
        <div class="row-w-img sec row">
            <div class="medium-6 columns ">
                <?php if(get_sub_field('first_image')) :
                    $img = get_sub_field('first_image');
                    $img_id = get_attachment_id($img);
                    $img_meta = wp_get_attachment_metadata($img_id, true);
                    $opt_image = wp_image_add_srcset_and_sizes($img, $img_meta, $img_id);
                ?>
                    <figure class="<?php the_sub_field('first_image_alignment') ?>">
                        <img src="<?php echo $opt_image; ?>" alt="section-img2">
                    </figure>
                <?php endif; ?>
              <?php the_sub_field("first_description"); ?>
            </div>
            <div class="medium-6 columns">
                <?php if(get_sub_field('second_image')) :
                    $img = get_sub_field('second_image');
                    $img_id = get_attachment_id($img);
                    $img_meta = wp_get_attachment_metadata($img_id, true);
                    $opt_image = wp_image_add_srcset_and_sizes($img, $img_meta, $img_id);
                ?>
                    <figure class="<?php the_sub_field('second_image_alignment') ?>">
                        <img src="<?php echo $opt_image; ?>" alt="section-img2">
                    </figure>
                <?php endif; ?>
                <?php the_sub_field("second_description"); ?>
            </div>
        </div>
		<?php elseif(get_row_layout() == "three_columns_content"): ?>
		 <!-- Row with 3 Images -->
        <section class="sec-w-bg <?php if( get_sub_field('background_color') == 'grey' ) echo ' bg_grey-lt'; ?>">
            <div class="row-w-img sec row">
                <div class="medium-4 columns">
                    <?php if(get_sub_field('third_image')) :
                        $img = get_sub_field('third_image');
                        $img_id = get_attachment_id($img);
                        $img_meta = wp_get_attachment_metadata($img_id, true);
                        $opt_image = wp_image_add_srcset_and_sizes($img, $img_meta, $img_id);
                    ?>
                        <figure>
                            <img src="<?php echo $opt_image; ?>"" alt="section-img2">
                        </figure>
                    <?php endif; ?>
                    <?php the_sub_field("third_description"); ?>
                </div>
                <div class="medium-4 columns">
                    <?php if(get_sub_field('fourth_image')) :
                        $img = get_sub_field('fourth_image');
                        $img_id = get_attachment_id($img);
                        $img_meta = wp_get_attachment_metadata($img_id, true);
                        $opt_image = wp_image_add_srcset_and_sizes($img, $img_meta, $img_id);
                    ?>
                        <figure>
                            <img src="<?php echo $opt_image; ?>"" alt="section-img2">
                        </figure>
                    <?php endif; ?>
                    <?php the_sub_field("fourth_description"); ?>
                </div>
                <div class="medium-4 columns">
                    <?php if(get_sub_field('fifth_image')) :
                        $img = get_sub_field('fifth_image');
                        $img_id = get_attachment_id($img);
                        $img_meta = wp_get_attachment_metadata($img_id, true);
                        $opt_image = wp_image_add_srcset_and_sizes($img, $img_meta, $img_id);
                    ?>
                        <figure>
                            <img src="<?php echo $opt_image; ?>"" alt="section-img2">
                        </figure>
                    <?php endif; ?>
                    <?php the_sub_field("fifth_description"); ?>
                </div>
            </div>
        </section>
		<?php elseif(get_row_layout() == "text_with_grey_background"): ?>
		 <!--Light Grey Background Section -->
        <section class="sec-w-bg bg_grey-lt">
            <div class="row">
                <div class="large-8 small-12 small-centered large-centered columns">
                    <div class="featured-box-text wo-bdr in-vc">
                       <?php the_sub_field("grey_text"); ?>
                    </div>
                </div>
            </div>
        </section>
		<?php endif; ?>

<?php endwhile; ?>