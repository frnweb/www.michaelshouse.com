<?php
/**
* Template Name: Home Page
*/
get_header();
?>
<?php

	$the_query = new WP_Query( 'page_id=87' );

	$the_query->have_posts();

	$the_query->the_post();

	$title=get_the_title();

	$image = wp_get_attachment_url(get_post_thumbnail_id(87));

	$content=get_the_content();
?>
  <!-- Main Content -->
    <div class="main-content">
        <!-- Banner -->
        <section class="banner">
            <!-- Banner Caption -->
            <div class="caption">
                <div class="accreditation"><img alt="CARF Accredited"src="<?php bloginfo('stylesheet_directory'); ?>/images/accreditations/CARF_logo.png"></div>
               <?php echo $content;?>
            </div>
        </section>
        <!-- Banner Ends -->
        <section class="featured">
            <!-- Start the Journey Today -->
            <div class="row ">
                <div class="small-12 large-10 small-centered large-centered  info-box-detail">
                    <div class="info-box neg column">
                        <div class="medium-7 columns">
                            <div class="box-text">
                               <?php echo get_field("home_description",87);?>
                            </div>
                        </div>
                        <div class="medium-5 columns">
                            <div class="box-button"> <a href="<?php echo get_field("button_link",87);?>" class="btn btn-primary text-uppercase"><!--<span class="badge">1</span>--><?php echo get_field("button_text",87);?></a> </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start the Journey Today Ends -->
            <div class="row">
                <!-- About Michael -->
                <div class="featured-box">
                    <div class="why-michael-house">
                        <div class="columns medium-6">
                            <div class="featured-box-text">
                                <small class="text-uppercase"><?php echo get_field("about_title",87);?></small>
                               <?php echo get_field("about_description",87);?>
                            </div>
                        </div>
                        <div class="columns medium-6">
                            <div class="featured-box-img">
                                <div class="orbit" role="region" data-orbit data-options="autoPlay:false;">
                                    <ul class="orbit-container">
									<?php if( have_rows('slider_images',87) ):  
										  $i=1;	
										  while( have_rows('slider_images',87) ): the_row(); 

                                          $slider_image = get_sub_field('slide_image');
                                          $slider_image_id = get_attachment_id($slider_image); 
                                          $slider_image_alt = get_post_meta($slider_image_id, '_wp_attachment_image_alt', true);
                                          $slider_image_meta = wp_get_attachment_metadata($slider_image_id, true);
                                          $opt_slider_image = wp_image_add_srcset_and_sizes($slider_image, $slider_image_meta, $slider_image_id);
									?>
                                        <li class="<?php if($i==1):?>is-active<?php endif;?> orbit-slide"> 
                                            <!-- <img src="<?php the_sub_field("slide_image");?>" alt="<?php echo $slider_image_alt; ?>"> -->
                                            <img src="<?php echo $opt_slider_image; ?>" alt="<?php echo $slider_image_alt; ?>">
                                        </li>
										 <?php  $i++; endwhile;  endif; ?>
                                       
                                    </ul>
                                    <nav class="orbit-bullets">
									<?php if( have_rows('slider_images',87) ):  
										   $j=0;	
										  while( have_rows('slider_images',87) ): the_row(); 
									?>
                                        <button <?php if($j==0):?>class="is-active"<?php endif;?> data-slide="<?php echo $j;?>"><?php if($j==0):?><span></span><?php endif;?></button>
										 <?php $j++; endwhile;  endif; ?>
                                        
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About Michael Ends -->

            <div class="row show-for-small-only">
                    <div class="columns small-12">
                        <div class="test-slider">
                        <?php
                                    $args = array(
                                        'posts_per_page' => -1,
                                        'order' => 'ASC',
                                        'post_type' => 'testimonial',
                                        'post_status' => 'publish',
                                        'suppress_filters' => true);
                                    $myposts = get_posts($args);
                                    foreach ($myposts as $post) : setup_postdata($post);
                                        ?>
                                    <div class="">
                                        <!-- Testimonial Item -->
                                        <div class="">
                                            <div class="columns medium-6">
                                                <div class="featured-box-img">
                                                    <!-- <div class="flex-video">
                                                        <iframe width="567" height="365" src="<?php the_field("video_link");?>" frameborder="0" allowfullscreen></iframe>
                                                    </div> -->
                                                    <div class="flex-video">
                                                        <object>
                                                            <param name="movie" value="<?php the_field('video_link'); ?>"></param>
                                                            <embed width="567" height="365" src="<?php the_field('video_link'); ?>" type="application/x-shockwave-flash"></embed>
                                                        </object>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="columns medium-6">
                                                <div class="featured-box-text info-box">
                                                    <blockquote><?php the_content();?></blockquote>
                                                    <p class="big"><?php the_title();?></p>
                                                    <small><?php the_field("designation");?></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; wp_reset_postdata();?>
                        </div>
                    </div>
            </div>

            <div class="row show-for-medium">
                <div class="featured-box video">
                    <!-- Testimonial -->

                        <div class="columns medium-12">
                            <div class="orbit" data-orbit data-timer-delay="10000">
                                <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span> &#9664;&#xFE0E;</button>
                                <button class="orbit-next"><span class="show-for-sr">Next Slide</span> &#9654;&#xFE0E;</button>
                                <ul class="orbit-container">
								 <?php
									$args = array(
										'posts_per_page' => -1,
										'order' => 'ASC',
										'post_type' => 'testimonial',
										'post_status' => 'publish',
										'suppress_filters' => true);
									$myposts = get_posts($args);
									foreach ($myposts as $post) : setup_postdata($post);
										?>
                                    <li class="is-active orbit-slide">
                                        <!-- Testimonial Item -->
                                        <div class="">
                                            <div class="columns medium-6">
                                                <div class="featured-box-img">
                                                    <!-- <div class="flex-video">
													   <iframe width="567" height="365" src="<?php the_field("video_link");?>" frameborder="0" allowfullscreen></iframe>
												    </div> -->
                                                    <div class="flex-video">
                                                        <object>
                                                            <param name="movie" value="<?php the_field('video_link'); ?>"></param>
                                                            <embed width="567" height="365" src="<?php the_field('video_link'); ?>" type="application/x-shockwave-flash"></embed>
                                                        </object>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="columns medium-6">
                                                <div class="featured-box-text info-box">
                                                    <blockquote><?php the_content();?></blockquote>
                                                    <p class="big"><?php the_title();?></p>
                                                    <small><?php the_field("designation");?></small>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
									<?php endforeach; wp_reset_postdata();?>
                                    
                                </ul>
                            </div>
                        </div>

                </div>
            </div>
            <!-- Testimonial Ends -->
        </section>

        <!-- Map Section -->
        <div class="center-location">
            <?php echo do_shortcode('[mapplic id="4" h="900"]');?>
        </div>
        <!-- Work With -->
        <!-- <section class="work-with">
            <div class="row">
                <div class="small-12 medium-10 small-centered large-centered columns">
                    <h3 class="text-center">We work with most insurances</h3>
                    <ul>
						<?php //if( have_rows('brand_logo',87) ):  
										  
							 //while( have_rows('brand_logo',87) ): the_row(); 

                                //$image = get_sub_field('b_image');
                                //$image_id = get_attachment_id($image); 
                                //$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', true);
							?>
                        <li>
                            <figure><img src="<?php //the_sub_field("b_image");?>" alt="<?php //echo $image_alt; ?>"></figure>
                        </li>
						 <?php //endwhile;  endif; ?>
                    </ul>
                    <a href="<?php //the_field("learn_more_link",87);?>" class="btn btn-default">Learn More</a> </div>
            </div>
        </section> -->
       
<?php get_footer();?>
