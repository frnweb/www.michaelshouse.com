<?php

/**

* Template Name: Resources 

*/

get_header();

?>

     <div class="main-content">
        <!-- Innerpage Banner -->
        <?php while ( have_posts() ) : the_post();
         $feature_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );  
         if( ! $feature_image ) {
              $feature_image = ot_get_option("default_header_image");
          }
        ?>
        <section class="banner banner-innerpage" style="background-image:url(<?php echo $feature_image;?>);">
            <!-- Innerpage Banner Caption -->
            <div class="caption">
                <h1><?php the_title();?></h1>
            </div>
        </section>
    <?php endwhile;?>
        <!-- Banner Ends -->
        <!-- Content Section -->
        <div class="row">
            <!-- Left Section -->
            <!-- Accordion Section -->
            <div class="small-12 large-9 columns">
                <!-- Alphabetical order -->
               
                <?php 
				$alphabets = array("1","2","3","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
				if( have_rows('accordion_list') ): ?>
                 <ul class="pagination text-center alphabetical" role="navigation" aria-label="Pagination">
                  <?php $j=1;   while( have_rows('accordion_list') ): the_row(); $value[] = get_sub_field('cat_title',$post->ID); endwhile;
				  
				  foreach($alphabets as $k => $alpha):
				 if(in_array($alpha,$value)){
				 ?>
                   <li class="<?php if($j==1){?>current<?php }?> active"><a href="#<?php echo $alpha;//get_sub_field('cat_title',$post->ID);?>" class="smoothscroll" id="alpha_anchor_<?=$alpha;?>"><?php echo $alpha;//get_sub_field('cat_title',$post->ID);?></a></li>
				   <?php
				   $j++;
				 }else{?>
					  <li class="disable"><a href="#<?php echo $alpha;//get_sub_field('cat_title',$post->ID);?>" class="smoothscroll"><?php echo $alpha;//get_sub_field('cat_title',$post->ID);?></a></li>
				<?php }?>
               <?php endforeach;
			   ?>
               </ul>

               <?php endif;?>

                <div class="sec">
                   
                        <!-- Accordion Item -->
                    <?php $i=0; $k=1;if( have_rows('accordion_list') ):?> 
                     <ul class="accordion resources" data-accordion data-allow-all-closed="true">
                     <?php while( have_rows('accordion_list') ): the_row();
                       $cat_title= get_sub_field('cat_title',$post->ID);
                      if( have_rows('section_content') ): 
                      while( have_rows('section_content') ): the_row();
                      $title=get_sub_field('title',$post->ID);
                      $content=get_sub_field('content',$post->ID);
                      if($i==0){ ?>
                        <li class="accordion-item <?php if($k==1){?>is-active<?php }?>" id="<?php echo  $cat_title; ?>">
                       <?php } else{?>
                         <li class="accordion-item <?php if($k==1){?>is-active<?php }?>">
                        <?php }?>
                            <a href="#" class="accordion-title" role="tab" aria-expanded="false" aria-selected="false"><?php echo $title;?></a>
                            <div class="accordion-content callout large" data-tab-content="" role="tabpanel"  aria-hidden="true">
                                <?php echo $content;?>
                            </div>
                        </li>
                    <?php $i++; $k++;endwhile; endif;?>
                    <?php $i=0; endwhile; ?>  
                    </ul>
                <?php endif;?>
                </div>
            </div>
            <!-- Sidebar Section -->
            <aside class="small-12 large-3 columns">
                <?php dynamic_sidebar('sidebar-1');?>
            </aside>
        </div>
    </div>
<?php get_footer();?>