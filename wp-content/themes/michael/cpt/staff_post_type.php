<?php
add_action('init', 'staff_post_type');

  function staff_post_type() {
	  
	    $rewrite = array(
		'slug'                  => 'staff',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);

  register_post_type('staff', array(

   'labels' => array(

   'name' => 'Staff',

   'singular_name' => 'Staff',

   'add_new' => 'Add Member',

   'edit_item' => 'Edit Member',

   'new_item' => 'New Member',

   'view_item' => 'View Member',

   'search_items' => 'Search Staff post',

    'not_found' => 'No Member found',

    'not_found_in_trash' => 'No Member in trash'

  ),


	'rewrite' => $rewrite,
    'public' => true,
    'menu_icon' => 'dashicons-groups',
    'supports' => array( 'title', 'editor', 'author', 'thumbnail','page-attributes' ),
    'taxonomies' => array('staff-cat')

    ));

	


 }///end custom post type

  add_action( 'init', 'create_staff_taxonomies', 0 );

 function create_staff_taxonomies()

 {

   // Add new taxonomy, make it hierarchical (like categories)

   $labels = array(

     'name' => _x( 'Staff Category', 'taxonomy general name' ),

     'singular_name' => _x( 'Staff Category', 'taxonomy singular name' ),

     'search_items' =>  __( 'Search Staff Category' ),

     'all_items' => __( 'All Staff Category' ),

     'parent_item' => __( 'Parent Staff Category' ),

     'parent_item_colon' => __( 'Parent Staff Category' ),

     'edit_item' => __( 'Edit Staff Category' ),

     'update_item' => __( 'Update Staff Category' ),

     'add_new_item' => __( 'Add New Staff Category' ),

     'new_item_name' => __( 'New Staff Category Name' ),

   );

   register_taxonomy('staff-cat',array('staff'), array(

     'hierarchical' => true,

     'labels' => $labels,

     'show_ui' => true,

     'query_var' => true,

     'rewrite' => array( 'slug' => 'staff-cat' ),
	 
      
   ));

 }

 
function location_taxonomy_init() {
    register_taxonomy(
        'staff-location',
        array('staff'),
        array(
          'hierarchical' => true,
            'label' => __( 'Location' ),
            'rewrite' => array( 'slug' => 'staff-location', 'ep_mask' => EP_PERMALINK ),
        )
    );
}
add_action( 'init', 'location_taxonomy_init');
  

 /* Do something with the data entered */

	add_action( 'save_post', 'project_save_postdata' );

    

  

    /* When the post is saved, saves our custom data */

   function project_save_postdata($post_id) {

  // verify if this is an auto save routine. 

  // If it is our form has not been submitted, so we dont want to do anything

  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 

      return;



  

  // Check permissions

  if ( 'page' == $_POST['post_type'] ) 

  {

    if ( !current_user_can( 'edit_page', $post_id ) )

        return;

  }

  else

  {

    if ( !current_user_can( 'edit_post', $post_id ) )

        return;

  }



  

}



add_filter( 'manage_edit-project_columns', 'my_edit_projects_columns' ) ;

function my_edit_projects_columns($columns) {



	$columns = array(

		'cb' => '<input type="checkbox" />',

		'title' => __( 'Title' ),

		'staff-cat' => __('Staff Category' ),

		'date' => __( 'Date' )

	);

	return $columns;

}



add_action( 'manage_project_posts_custom_column', 'my_manage_projects_columns', 10, 2 );



function my_manage_projects_columns( $columns, $post_id ) {

   //global $wpdb;

	global $post;

	

	if($columns=='staff-cat'){

		/* Get the genres for the post. */

			$terms = get_the_terms( $post_id, 'staff-cat' );

			//print_r($terms);die;

			

			/* If terms were found. */

			if ( !empty( $terms ) ) {

				$out = array();

				/* Loop through each term, linking to the 'edit posts' page for the specific term. */

				foreach ( $terms as $term ) {

					$out[] = sprintf( '<a href="%s">%s</a>',

						esc_url( add_query_arg( array( 'post_type' =>$post->post_type, 'staff-cat' => $term->slug ), 'edit.php' ) ),

						esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'staff-cat', 'display' ))

						

						);

				}

				/* Join the terms, separating them with a comma. */

				echo join( ', ', $out );

			}

		}


	else{}

}

?>