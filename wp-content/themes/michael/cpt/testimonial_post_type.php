<?php
add_action('init', 'testimonial_post_type');

  function testimonial_post_type() {

  register_post_type('testimonial', array(

   'labels' => array(

   'name' => 'Testimonials',

   'singular_name' => 'Testimonials',

   'add_new' => 'Add Testimonial',

   'edit_item' => 'Edit Testimonial',

   'new_item' => 'New Testimonial',

   'view_item' => 'View Testimonial',

   'search_items' => 'Search Testimonial post',

    'not_found' => 'No Testimonial found',

    'not_found_in_trash' => 'No Testimonial in trash'

  ),



    'public' => true,
     'menu_icon' => 'dashicons-testimonial',
    'supports' => array( 'title', 'editor', 'thumbnail','page-attributes' )


    ));



 }



?>