<?php
/**
* Template Name: Blog Template
*/
get_header();
?>
   <!-- Main Content -->
    <div class="main-content">
        <?php 
        $featured_image = ot_get_option("blog_header_image");
        if( ! $feature_image ) {
              $feature_image = ot_get_option("default_header_image");
          }
        ?>
        <section class="banner banner-innerpage" style="background-image:url(<?php echo $feature_image;?>);">
            <!-- Innerpage Banner Caption -->
            <div class="caption">
                <h1>Michael's House Blog</h1>
            </div>
        </section>
        <!-- Content Section -->
        <div class="row">
            <!-- Left Section -->
            <div class="large-9 small-12 columns">
                <!-- Blog Article -->
				<?php   $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
                $blogarg=array('post_type'=>'post','paged' => $paged,'posts_per_page' => 10);

                query_posts($blogarg);
                while ( have_posts() ) : the_post();
                  $recent_id=get_the_id();
                  $feature_image = wp_get_attachment_url( get_post_thumbnail_id($recent_id) );                                    
          ?>
                <article>
				
                    <div class="row">
					<?php if($feature_image):?>
                        <div class="medium-7 columns">
                            <small><?php $categories = get_the_category();
							if ( ! empty( $categories ) ) {?>
								<a href="<?php echo get_category_link($categories[0]->term_id);?>"><?php echo esc_html( $categories[0]->name );   ?></a><?php
							}?></small>
                            <h1><a href="<?php the_permalink();?>"><?php the_title();?></a></h1>
                            <p><?php the_excerpt(); ?></p>
 			                <a href="<?php the_permalink(); ?>">Read More</a>
                        </div>
                        <div class="medium-5 columns hide-for-small-only">
                            <figure>
                                <a href="<?php the_permalink(); ?>"><img src="<?php echo $feature_image; ?>" alt="<?php the_title();?>"></a>
                            </figure>
                        </div>
						<?php else:?>
						 <div class="medium-12 columns">
                           <small><?php $categories = get_the_category();
							if ( ! empty( $categories ) ) {?>
								<a href="<?php echo get_category_link($categories[0]->term_id);?>"><?php echo esc_html( $categories[0]->name );   ?></a><?php
							}?></small>
                            <h1><a href="<?php the_permalink();?>"><?php the_title();?></a></h1>
                            <p><?php the_excerpt(); ?></p>
                        </div>
					<?php endif;?>
                    </div>
					
                </article>
				<?php endwhile;?>
               
                <!-- Pagination -->
				<?php if(function_exists('wp_paginate')) {
				    wp_paginate();
				}?>
            </div>
            <!-- Sidebar Section -->
            <aside class="large-3  small-12 columns hide-for-small-only">
                 <?php dynamic_sidebar('sidebar-1');?>
            </aside>
        </div>
    </div>
<?php get_footer();?>    
