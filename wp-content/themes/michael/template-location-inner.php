<?php
/**
 * Template Name: Inner Location Template
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); $id = get_the_ID();?>
<?php while ( have_posts() ) : the_post();
$feature_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );  
if( ! $feature_image ) {
    $feature_image = ot_get_option("default_header_image");
}
?>
    <!-- Main Content -->
    <div class="main-content">
        <!-- Innerpage Banner -->
        <section class="banner banner-innerpage" style="background-image:url(<?php echo $feature_image;?>);">
            <!-- Innerpage Banner Caption -->
            <div class="caption">
                <h1><?php the_title();?></h1>
            </div>
        </section>
        <!-- Banner Ends -->
        <!-- Content Section -->
        <!-- Dark Blue Bg Section -->
        <div class="blue-row-bg">
            <ul>
                <!-- Row with Background -->
                <li class="bg_list">
                    <div class="row">
                        <div class="medium-6 columns ">
                                <small><?php the_title();?></small>
                               <?php the_content();?>
                            </div>
                            
                            <div class="medium-6 columns video-slider ">
                               
                                    <?php echo do_shortcode(get_field("shortcode"));?>
                                
                            </div>
                           
                      
                    </div>
                </li>
            </ul>
        </div>
        <!--Light Grey Background Section -->
        <?php echo get_field("large_description");?>
        <?php endwhile;?>

        <!-- Content Section -->
        <?php while(the_flexible_field("content")): ?>
        <?php if(get_row_layout() == "text"): ?>
        <div class="row sec">
            <div class="small-12 medium-10 small-centered large-centered columns">
               
                   <?php the_sub_field("block_content"); ?>
               
            </div>
        </div>
        <?php elseif(get_row_layout() == "image_content"): ?>
        <div class="row sec" data-equalizer>
            <div class=" mb-0">
                <div class="medium-6 columns out-vc" data-equalizer-watch>
                    <div class="featured-box-img in-vc">
                        <figure class="">
                            <img src="<?php the_sub_field("image"); ?>" alt="section-img">
                        </figure>
                    </div>
                </div>
                <div class="medium-6 columns out-vc" data-equalizer-watch>
                    <div class="featured-box-text wo-bdr in-vc">
                        <?php the_sub_field("description"); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php elseif(get_row_layout() == "two_columns_content"): ?>
         <!-- Row with 2 Images -->
        <div class="row-w-img sec row">
            <div class="medium-6 columns ">
                <?php if(get_sub_field('first_image')): ?>
                    <figure>
                        <img src="<?php the_sub_field("first_image"); ?>" alt="section-img2">
                    </figure>
                <?php endif; ?>
              <?php the_sub_field("first_description"); ?>
            </div>
            <div class="medium-6 columns">
                <?php if(get_sub_field('second_image')): ?>
                    <figure>
                        <img src="<?php the_sub_field("second_image"); ?>" alt="section-img2">
                    </figure>
                <?php endif; ?>
                <?php the_sub_field("second_description"); ?>
            </div>
        </div>
        <?php elseif(get_row_layout() == "three_columns_content"): ?>
         <!-- Row with 3 Images -->
        <section <?php echo get_sub_field('background_color') == "grey" ? 'class="sec-w-bg bg_grey-lt"' : ''; ?>>
            <div class="row-w-img sec row">
                <div class="medium-4 columns">
                    <?php if(get_sub_field('third_image')): ?>
                        <figure>
                            <img src="<?php the_sub_field("third_image"); ?>" alt="section-img2">
                        </figure>
                    <?php endif; ?>
                    <?php the_sub_field("third_description"); ?>
                </div>
                <div class="medium-4 columns">
                    <?php if(get_sub_field('fourth_image')): ?>
                        <figure>
                            <img src="<?php the_sub_field("fourth_image"); ?>" alt="section-img2">
                        </figure>
                    <?php endif; ?>
                    <?php the_sub_field("fourth_description"); ?>
                </div>
                <div class="medium-4 columns">
                    <?php if(get_sub_field('fifth_image')): ?>
                        <figure>
                            <img src="<?php the_sub_field("fifth_image"); ?>" alt="section-img2">
                        </figure>
                    <?php endif; ?>
                    <?php the_sub_field("fifth_description"); ?>
                </div>
            </div>
        </section>
        <?php elseif(get_row_layout() == "text_with_grey_background"): ?>
         <!--Light Grey Background Section -->
        <section class="sec-w-bg bg_grey-lt">
            <div class="row">
                <div class="large-8 small-12 small-centered large-centered columns">
                    <div class="featured-box-text wo-bdr in-vc">
                       <?php the_sub_field("grey_text"); ?>
                    </div>
                </div>
            </div>
        </section>
        <?php endif; ?>

    <?php endwhile; ?>

        <!-- Stabilization Staff -->
        <div class=" text-center staff-sec sec info-box">
            <div class="row">
                <h3>Staff Highlights</h3>
                <p>Our staff is specially trained in the Foundations Recovery Network treatment model to address the complex needs of individuals with co-occurring disorders and to treat the whole person in a comprehensive way.</p>
                <?php $terms=get_field('choose_our_staff',$post->ID);
                //print_r($terms);
                $term=get_term_by('id', $terms[1]->term_id,$terms[1]->taxonomy);?>
                <?php 
                query_posts(array('post_type'=>'staff','posts_per_page'=>-1, 'tax_query' => array(array('taxonomy' =>$terms[0]->taxonomy,'field' => 'id',  'terms' =>$terms[0]->term_id ))));
                if ( have_posts() ) : 
                ?>
                <ul class="staff-list">
                    <?php while ( have_posts() ) : the_post();
                    $feature_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );  ?>
                    
                    <li class="hide-for-small-only">
                     <?php if($feature_image){?>
                        <figure>
                            <a href="<?php the_permalink();?>"><img src="<?php echo  $feature_image; ?>" alt="<?php the_title();?>"></a>
                        </figure>
                        <?php }?>
                        <a href="<?php echo get_the_permalink($post->ID); ?>"><?php the_title();?></a>
                        <p><?php the_field('designation');?></p>
                    </li>
                     <?php endwhile; wp_reset_query();?>
                </ul>
                <?php endif ?>
                <a href="<?php echo get_permalink(17436);?>" class="btn btn-default">Meet the Entire Team</a>
            </div>
        </div>
        <!-- Address Section -->
        <div class="featured center-mapp">
            <div class="row">
                <div class="medium-5 columns out-vc text-center">
                    <div class="info-box address in-vc text-center">
                        <?php echo get_field("address",$id);?>
                    </div>
                </div>
            </div>
            <div class="map-section">
                <div class="flex-video widescreen">
                   <?php echo get_field("iframe_code",$id);?>
                </div>
            </div>
        </div>
    </div>
   <?php get_footer();?>
