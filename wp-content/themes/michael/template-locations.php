<?php
/**
* Template Name: Locations
*/
get_header();
?>
<?php while ( have_posts() ) : the_post();
$feature_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );  
if( ! $feature_image ) {
    $feature_image = ot_get_option("default_header_image");
}
?>
    <!-- Main Content -->
    <div class="main-content">
        <!-- Innerpage Banner -->
        <section class="banner banner-innerpage" style="background-image:url(<?php echo $feature_image;?>);">
            <!-- Innerpage Banner Caption -->
            <div class="caption">
                <h1><?php the_title();?></h1>
            </div>
        </section>
        <!-- Banner Ends -->
        <!-- Content Section -->
        <div class="row sec row-w-img-text">
          
            <div class="medium-6 columns float-right">
                    <small><?php the_title();?></small>
                   <?php the_content();?>
           
            </div>
              <div class="medium-6 columns  ">
              
                    <div class="flex-video">
						<iframe width="567" height="314" src="<?php the_field("location_video_link");?>" frameborder="0" allowfullscreen></iframe>
					</div>
            
            </div>
        </div>
        <!-- Dark Blue Bg Section -->
        <div class="blue-row-bg">
            <ul>
                <!-- Row with Background -->
				<?php if( have_rows('locations_description') ):  
									   	  
							$a = 0; while( have_rows('locations_description') ): the_row(); 
                                $class = ['',''];
                                if( $a % 2 ) $class = ['medium-push-6', 'medium-pull-6'];

                                $location_image = get_sub_field('location_image');
                                $location_image_id = get_attachment_id($location_image); 
                                $location_image_meta = wp_get_attachment_metadata($location_image_id, true);
                                $opt_image = wp_image_add_srcset_and_sizes($location_image, $location_image_meta, $location_image_id);
							?>
                <li class="bg_list">
                    <div class="row">
                        <div class="featured-box">
                            <div class="medium-6 <?php echo $class[0] ?> columns out-vc" data-equalizer-watch>
                                <div class="featured-box-img in-vc">
                                    <figure class="">
                                        <a href="<?php the_sub_field('location_button_link'); ?>">
                                            <!-- <img src="<?php the_sub_field("location_image");?>" alt="<?php the_sub_field("location_title");?>"> -->
                                            <img src="<?php echo $opt_image; ?>" alt="<?php the_sub_field("location_title");?>">
                                        </a>
                                    </figure>
                                </div>
                            </div>
                            <div class="medium-6 <?php echo $class[1] ?> columns out-vc" data-equalizer-watch>
                                <div class="featured-box-text wo-bdr in-vc">
                                    <a href="<?php the_sub_field('location_button_link'); ?>">
                                        <h3><?php the_sub_field("location_title");?></h3>
                                    </a>
                                    <div class="hide-for-small-only"><?php the_sub_field("location_content");?></div>
                                    <a href="<?php the_sub_field("location_button_link");?>" class="btn btn-default">More Information</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
				<?php $a++; endwhile;  endif; ?>
                <!-- Row with Background -->
				
            </ul>
        </div>
        <!-- Map Section -->
        <div class="center-location">
            <?php echo do_shortcode('[mapplic id="4" h="900"]');?>
        </div>
<?php endwhile;?>
<?php get_footer();?>    
