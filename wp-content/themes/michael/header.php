<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="shortcut icon" href="<?php echo ot_get_option("favicon"); ?>" type="image/x-icon">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->


	<?php wp_head(); ?>
	<meta property="og:image" content="http://www.michaelshouse.com/wp-content/uploads/Michaels-House-Banner-for-Social.jpg" />
	<meta property="og:image:width" content="1200" />
	<meta property="og:image:height" content="717" />

	<!-- VOB reCAPTCHA CDN -->
	<script src="https://www.google.com/recaptcha/api.js"></script>

</head>

<body <?php if(is_single()): body_class("archive"); else: body_class(); endif; ?>>

 <!-- Header-->
    <header class="site-header" id="masthead">
        <div class="top-bar" id="mobile-stick" data-sticky-container>
            <div class="sticky" data-sticky data-margin-top="0">
                <div class="top-bar-title"><span class="menu-toggle" data-responsive-toggle="responsive-menu" data-hide-for="large">
                <button class="menu-icon" type="button" data-toggle="">MENU</button></span>
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo" title="Michael House">
					<?php if(is_single() || is_category() ):?>

						<?php 
							$classes = get_body_class();
							if( in_array('staff-template-default', $classes) ):?>
							<img src="<?php echo ot_get_option("header_logo"); ?>" alt="Michael's House" class="b-fix"> 
							<img src="<?php echo ot_get_option("sticky_header_logo"); ?>" alt="Michael's House" class=" a-fix"></a>
						<?php else:?>
							<img src="<?php echo ot_get_option("sticky_header_logo"); ?>" alt="Michael's House" class="b-fix"> 
							<img src="<?php echo ot_get_option("sticky_header_logo"); ?>" alt="Michael's House" class=" a-fix"></a>
						<?php endif;?>

					<?php else:?>
						<img src="<?php echo ot_get_option("header_logo"); ?>" alt="Michael's House" class="b-fix"> 
						<img src="<?php echo ot_get_option("sticky_header_logo"); ?>" alt="Michael's House" class=" a-fix"></a>
					<?php endif;?>
                </div>
                <div id="responsive-menu">
                    <div class="top-bar-right is-visible-desktop">
                        <div class="phone-sec bg_red text-center"><p>CALL: </p><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Desktop Header"]'); ?><?php //echo ot_get_option("phone"); ?>
                        </div>
                        <nav class="main-menu">
						<?php wp_nav_menu(array('container' => '', 'menu' => 'top_menu', 'menu_class' => 'menu useful hide-for-small-only clearfix')); ?>
						<?php wp_nav_menu(array('container' => '', 'menu' => 'main_menu', 'menu_class' => 'menu clearfix','theme_location' => 'primary')); ?>
                        </nav>
                    </div>
					<div class="top-bar-right is-visible-mobile">
					<nav class="main-menu">
					
					<ul class="sf-menu menu-mobile" id="example">
                
						 <?php $args = array(
						'order'                  => 'ASC',
						'orderby'                => 'menu_order',
						'post_type'              => 'nav_menu_item',
						'post_status'            => 'publish',
						'output'                 => ARRAY_A,
						'output_key'             => 'menu_order',
						'nopaging'               => true,
						'update_post_term_cache' => false ); ?> 
						<?php $items = wp_get_nav_menu_items( 'main_menu' ); ?>
						<?php foreach ($items as $_menu) : ?>
							
						
							   <?php $parent_id = $_menu->ID; ?> 
								<?php if($_menu->menu_item_parent == 0): $i=0; ?>
								
							<li><a href="<?php echo $_menu->url; ?>"><?php echo $_menu->title; ?></a>
							
							<ul>
								
								<?php foreach ($items as $_menu) :  ?>
								
                            <?php if($_menu->menu_item_parent == $parent_id ): ?>
							
							 <?php $parent_id_1 = $_menu->ID; ?> 
									<li class="submenu_class">
										<a href="<?php echo $_menu->url; ?>"><?php echo $_menu->title; ?></a>
										
										<ul>
										<?php foreach ($items as $_menu) : ?>
										
											<?php if($_menu->menu_item_parent == $parent_id_1 ): ?>
											<li class="submenu_class">
												<a href="<?php echo $_menu->url; ?>"><?php echo $_menu->title; ?></a>
												
											</li>
											<?php endif; ?>
										<?php endforeach; ?>
										</ul>
									</li>
									<?php $i++; endif; ?>
							<?php endforeach; ?>
								</ul>
								
							</li>
							<?php endif;endforeach;?>
							<?php wp_nav_menu(array('container' => '', 'menu' => 'top_menu', 'menu_class' => 'menu useful clearfix')); ?>
						</ul>
					</nav>
					<div class="phone-sec bg_red text-center"> <?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Mobile Menu"]'); ?></a>
					</div>
				</div>
                </div>
            </div>
        </div>

    </header>
    <!-- Header Ends -->
 <!-- Info Button -->
    <div class="info-btn-sec">
        <div class="info-menu">
            <ul class="info__list">
                <li class="info__list__item info-btn-bg">
			<?php echo do_shortcode('[lhn_inpage button="chat" id="chat" text="empty" offline="email empty" category="Global Contact Options"]'); ?>
                </li>
                <li class="info__list__item info-btn-bg">
			<?php echo do_shortcode('[lhn_inpage button="email" id="mail" text="empty" category="Global Contact Options"]'); ?>
                </li>
                <li class="info__list__item info-btn-bg">
			<?php echo do_shortcode('[frn_phone id="phone" category="Global Contact Options" action="Phone Clicks on Icon" text="empty" desktop_url="/contact/" desktop_action="Desktop Clicks to Contact Us"]'); ?>
                </li>
            </ul>
        </div>
        <div class="info-btn info-btn-bg">
            Contact </div>
    </div>
    <!-- Info Button Ends -->
