<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<?php while ( have_posts() ) : the_post();
//$feature_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );  ?>
<?php if(has_post_thumbnail()) :
    $feature_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
else:
    $feature_image = ot_get_option("default_header_image");
endif; ?>
 <!-- Main Content -->
    <div class="main-content">
        <!-- Innerpage Banner -->
        <section class="banner banner-innerpage" style="background-image:url(<?php echo $feature_image;?>);">
            <!-- Innerpage Banner Caption -->
            <div class="caption">
                <h1><?php the_title();?></h1>
            </div>
        </section>
        <!-- Banner Ends -->
        <!-- Content Section -->
        <div class="row">
		<div class="medium-9 columns">
        <div id="content">
		<small><?php the_title();?></small>
            <?php the_content();?>
		</div>
        </div>
			 <!-- Sidebar Section -->
                <aside class="medium-3 columns">
                   
                    <?php dynamic_sidebar('sidebar-1');?>
                </aside>
        </div>
   
<?php endwhile;	?>

<?php
get_footer();
