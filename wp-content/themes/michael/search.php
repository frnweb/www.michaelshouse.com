<?php
/**
* Template Name: Blog Template
*/
get_header();
?>
   <!-- Main Content -->
    <div class="main-content">
        <?php $feature_image = ot_get_option("default_header_image"); ?>
        <!-- Innerpage Banner -->
        
        <section class="banner banner-innerpage " style="background-image:url(<?php echo $feature_image;?>);">
            <!-- Innerpage Banner Caption -->
            <div class="caption">
                <h1>Searching Michael's House</h1>
            </div>
        </section>
   
        <!-- Banner Ends -->
        <!-- Content Section -->
        <div class="row">
            <!-- Left Section -->
            <div class="large-9 small-12 columns">
                <!-- Blog Page Title -->
                <h2 class="pb-20"> Search Query: <strong><?php the_search_query(); ?></strong></h2>
                <!-- Blog Article -->
				<?php   

                if(have_posts()) :

                // query_posts($blogarg);
                while ( have_posts() ) : the_post();
                  $recent_id=get_the_id();
                  $feature_image = wp_get_attachment_url( get_post_thumbnail_id($recent_id) );                                    
                ?>
                <article>
				
                    <div class="row">
					<?php if($feature_image):?>
                        <div class="medium-7 columns">
                            <small><?php $categories = get_the_category();
							if ( ! empty( $categories ) ) {?>
								<a href="<?php echo get_category_link($categories[0]->term_id);?>"><?php echo esc_html( $categories[0]->name );   ?></a><?php
							}?></small>
                            <h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                            <p><?php the_excerpt(); ?></p>
 			                <a href="<?php the_permalink(); ?>">Read More</a>
                        </div>
                        <div class="medium-5 columns hide-for-small-only">
                            <figure>
                                <a href="<?php the_permalink(); ?>"><img src="<?php echo $feature_image; ?>" alt="<?php the_title();?>"></a>
                            </figure>
                        </div>
						<?php else:?>
						 <div class="medium-12 columns">
                           <small><?php $categories = get_the_category();
							if ( ! empty( $categories ) ) {?>
								<a href="<?php echo get_category_link($categories[0]->term_id);?>"><?php echo esc_html( $categories[0]->name );   ?></a><?php
							}?></small>
                            <h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
<?php  
$text = get_post_meta($post->ID, 'block_content', true);
$text = get_field('block_content', $post->ID);
print_r($text);
//echo $text;
?>
                            <p><?php the_excerpt(); ?></p>
                            <a href="<?php the_permalink();?>">Read More</a>
                        </div>
					<?php endif;?>
                    </div>
					
                </article>
				<?php endwhile; ?>

                <?php else : ?>

                <div class="search_results">

                    <p>We don't have any topics that relate to your search. Please try a different search or give us a call if you have questions.</p>

                    <p class="bold number"><?php echo do_shortcode('[frn_phone]'); ?></p>

                    <div class="frn_search_box"><?php echo get_search_form(false); ?></div>
                    <p style="margin: 20px 0 100px 0;">
                        We try to get everyone help for their addiction, no matter their financial status or situation. 
                        If you have questions, we are available 24/7.
                    </p>

                </div>

                <?php endif; ?>
               
                <!-- Pagination -->
				<?php if(function_exists('wp_paginate')) {
				    wp_paginate();
				}?>
            </div>
            <!-- Sidebar Section -->
            <aside class="large-3  small-12 columns hide-for-small-only">
                 <?php dynamic_sidebar('sidebar-1');?>
            </aside>
        </div>
    </div>
<?php get_footer();?>    
