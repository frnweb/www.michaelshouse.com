<?php



// Shortcodes based on Foundations 6.0

	// ROW
		function frn_foundation_row ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> 'small-collapse large-uncollapse medium-uncollapse'
				), $atts );
			return '<div class="row expanded content-shortcode-row ' . esc_attr($specs['style'] ) . '" data-equalizer data-equalize-on="medium">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('row', 'frn_foundation_row' );
	///ROW

	// COLUMN
		function frn_foundation_col ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'small'		=> '12',
				'medium'	=> '12',
				'large'		=> '12',
				), $atts );
			return '<div class="small-' . esc_attr($specs['small']) . ' medium-' . esc_attr($specs['medium']) . ' large-' . esc_attr($specs['large']) . ' columns">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('col', 'frn_foundation_col' );
	///COLUMN

	// BLOCKGRID
		function frn_foundation_blockgridul ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'screen'		=> 'medium',
				'columns'		=> '4',
				'altscreen'		=> '',
				'altcolumns'	=> '',
				), $atts );
			return '<div class="row ' . esc_attr($specs['screen'] ) . '-up-' . esc_attr($specs['columns']) . ' ' . esc_attr($specs['altscreen'] ) . '-up-' . esc_attr($specs['altcolumns']) .'">' . do_shortcode ( $content ) . '</div>';
		}

		add_shortcode ('blockgrid', 'frn_foundation_blockgridul' );
	///BLOCKGRID

	// BLOCKGRID LIST ITEM
		function frn_foundation_blockgridli ( $atts, $content = null ) {
			return '<div class="column column-block">' .  do_shortcode ( $content ) . '</div>';
		}

		add_shortcode ('item', 'frn_foundation_blockgridli' );
	///BLOCKGRID LIST ITEM

	// BUTTON
		function frn_foundation_buttons ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'url'	=> '#',
				'style'	=> '', 
				'target'	=> '_self'
				), $atts );
			if(in_array('read-next', $specs, true)){
				return '<div class="read-next"><h4>Read This Next:</h4><a href="' . esc_attr($specs['url'] ) . '" class="button secondary ' . esc_attr($specs['style'] ) . '" target="'. esc_attr($specs['target'] ) .'">' . $content . '</a></div>';
			} else {
			return '<a href="' . esc_attr($specs['url'] ) . '" class="button ' . esc_attr($specs['style'] ) . '" target="'. esc_attr($specs['target'] ) .'">' . $content . '</a>';
			};
		}

		add_shortcode ('button', 'frn_foundation_buttons' );
	///BUTTON

	// FLEXVIDEO
		function frn_foundation_flexvideo ( $atts, $content = null ) {
			return '<div class="flex-video">' . $content . '</div>';
		}
		add_shortcode ('flexvideo', 'frn_foundation_flexvideo' );
	///FLEXVIDEO

	// TABS
		function frn_foundation_tabs( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> ''
			), $atts );
			static $i = 0;
			$i++;
			
			return '<ul class="tabs ' . esc_attr($specs['style'] ) . '" data-tabs id="example-tabs' . $i . '">' . do_shortcode ( $content ) . '</ul>';
		}

		add_shortcode ('tabs', 'frn_foundation_tabs' );
	///TABS

	// TAB TITLE
		function frn_foundation_tabs_title ( $atts, $content = null ) {

		    $specs = shortcode_atts( array(
		        'class'     => ''
		    ), $atts );

		    static $i = 0;
		    if ( $i == 0 ) {
		        $class = 'is-active';
		    } else {
		        $class = NULL;
		    }
		    $i++;
		    $value = '<li class="tabs-title ' . $class . ' '.esc_attr($specs['class'] ).'"><a href="#tabpanel' . $i . '">' .
		    $content . '</a></li>';

		    return $value;
		}

		add_shortcode ('tab-title', 'frn_foundation_tabs_title' );
	///TAB TITLE

	// TAB CONTENT
		function frn_foundation_tabs_content( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> ''
			), $atts );
			static $i = 0;
			$i++;
			return '<div class="tabs-content ' . esc_attr($specs['style'] ) . '" data-tabs-content="example-tabs' . $i . '">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('tab-content', 'frn_foundation_tabs_content' );
	///TAB CONTENT

	// TAB PANEL
		function frn_foundation_tabs_panel ($atts, $content = null ) {
			static $i = 0;
			if ( $i == 0 ) {
				$class = 'is-active';
			} else {
				$class = NULL;
			}
			$i++;
			return '<div class="tabs-panel ' . $class .'" id="tabpanel' . $i . '">' . do_shortcode( $content ) . '</div>';
		}
		add_shortcode ('tab-panel', 'frn_foundation_tabs_panel' );
	///TAB PANEL

	// ACCORDION
		function frn_foundation_accordion( $atts, $content = null ) {
			return '<ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">' . do_shortcode ( $content ) . '</ul>';
		}
		add_shortcode ('accordion', 'frn_foundation_accordion' );
	///ACCORDION

	// ACCORDION ITEM
		function frn_foundation_tabs_accordion_item ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'title'		=> ''
				), $atts );
			static $i = 0;
		/*	if ( $i == 0 ) {
				$class = 'active';
			} else {
				$class = NULL;
			}*/
			$i++;
			$value = '<li class="accordion-item" data-accordion-item><a href="#" class="accordion-title">' . esc_attr($specs['title'] ) . '</a><div id="panel' . $i . '" class="accordion-content" data-tab-content>' . do_shortcode( $content ) . '</div></li>';

			return $value;
		}

		add_shortcode ('accordion-item', 'frn_foundation_tabs_accordion_item' );
	///ACCORDION ITEM


///THESE ARE SOME SHORTCODES THAT ARE INDEPENDENT OF THE FOUNDATION FRAMEWORK

	// DIVIDER
		function line_divider() {

			return '<div class="divider clearfix"></div>';

		}
		add_shortcode( 'divider', 'line_divider' );
	///DIVIDER

	// CALLOUT
		function callout_box ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> ''
				), $atts );
			return '<div class="callout ' . esc_attr($specs['style'] ) . '">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('callout', 'callout_box' );
	///CALLOUT

	// CALLOUT CARD
		function callout_card ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> '',
				'img'		=> ''
				), $atts );
			return '<div class="callout-card ' . esc_attr($specs['style'] ) . '"><div class="card-media" style="background-image: url('. esc_attr($specs['img'] ) .'"></div><div class="card-content">' . do_shortcode ( $content ) . '</div></div>';
		}
		add_shortcode ('callout-card', 'callout_card' );
	///CALLOUT CARD

	// LARGE TEXT
		function large_text ( $atts, $content = null ) {
			return '<div class="callout-text">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('large_text', 'large_text' );
	///LARGE TEXT

	// CALLOUT EQUALIZER
		function callout_box_equalizer ( $atts, $content = null ) {
			return '<div class="callout equalizer" data-equalizer-watch>' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('callout_equalizer', 'callout_box_equalizer' );
	///CALLOUT EQUALIZER

	// MAP
		function map_container ( $atts, $content = null ) {
			return '<div class="map-container">' . $content . '</div>';
		}
		add_shortcode ('map', 'map_container' );
	///MAP

	// CONTAINER
		function div_container ( $atts, $content = null ) {
		    $specs = shortcode_atts( array(
		        'class'     => '',
		        ), $atts );
		    return '<div class="container ' . esc_attr($specs['class']) . ' ">' .  do_shortcode ( $content ) . '</div>';
		}

		add_shortcode ('container', 'div_container' );
	///CONTAINER


//disables wp texturize on registered shortcodes
function frn_shortcode_exclude( $shortcodes ) {
    $shortcodes[] = 'row';
    $shortcodes[] = 'column';
    $shortcodes[] = 'blockgrid';
    $shortcodes[] = 'item';
    $shortcodes[] = 'button';
    $shortcodes[] = 'flexvideo';
    $shortcodes[] = 'tabs';
    $shortcodes[] = 'tab-title';
    $shortcodes[] = 'tab-content';
    $shortcodes[] = 'tab-panel';
    $shortcodes[] = 'accordion';
    $shortcodes[] = 'accordion-item';
    $shortcodes[] = 'modal';
	$shortcodes[] = 'divider';
	$shortcodes[] = 'map';
	$shortcodes[] = 'contact_box';
	$shortcodes[] = 'vob_form';
	$shortcodes[] = 'frn_privacy_url';
    $shortcodes[] = 'frn_footer';
    $shortcodes[] = 'lhn_inpage';
    $shortcodes[] = 'frn_phone';
    $shortcodes[] = 'frn_social';
    $shortcodes[] = 'frn_boxes';

    return $shortcodes;
}

add_filter ('no_texturize_shortcodes', 'frn_shortcode_exclude' );

// remove and re-prioritize wpautop to prevent auto formatting inside shortcodes
// shortcode_unautop is a core function

remove_filter( 'the_content', 'wpautop');
add_filter ( 'the_content', 'wpautop', 99 );
add_filter ('the_content', 'shortcode_unautop', 100 );


function my_acf_add_local_field_groups() {
    remove_filter('acf_the_content', 'wpautop' );
}
add_action('acf/init', 'my_acf_add_local_field_groups');