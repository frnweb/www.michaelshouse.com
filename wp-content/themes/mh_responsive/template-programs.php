<?php
/*
Template Name: Programs
*/
?>


<?php get_header(); ?>
<section class="banner treatment-programs">
	<h1 class="text-center">Treatment Programs<br><small>At Michael's House</small></h1>
	<div class="row" data-equalizer>
		<div class="large-6 large-uncentered medium-8 medium-centered columns" data-equalizer-watch>
			<div class="flex-video">
				<iframe width="560" height="315" src="//www.youtube-nocookie.com/embed/qGeyWsWNn58?rel=0&amp;controls=0" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
		<div class="large-6 columns" data-equalizer-watch>
			<p>Depending on which Michael’s House campus you are attending (the Stabilization Center, the Women’s Center, the Men’s Center or the Outpatient Center), you will be able to experience different types of groups and activities.</p>
			<p>The treatment program at Michael’s House is based on the award-winning system of care developed by its parent company, Foundations Recovery Network. It is designed to integrate care for both substance abuse and mental health conditions in a comprehensive way that allows for long-term healing and recovery.</p>
			<div class="text-center">
				<a href="http://www.michaelshouse.com/locations" class="button large round secondary"><span class="step">Step 4</span><span class="step-text">Where are we located?</span></a>
				<p class="helper">Learn all you need to know through a few easy steps</p>
			</div>				
		</div>
	</div>
	<?php include get_template_directory().'/social.php'; ?>
</section>
<section>
	<div class="border-block spacing">
		<div class="row">	
			<div class="large-12 columns">
				<h2 class="text-center">Each structured weekly program includes:</h2>
				<ul class="checks large-block-grid-3 medium-block-grid-2 small-block-grid-1">
					<li>Individual psychotherapy</li>
					<li>Primary therapy groups</li>
					<li>Dialectical Behavior Therapy</li>
					<li>Structured metaphor/experiential groups</li>
					<li>Expressive arts therapy</li>
					<li>Dual diagnosis groups</li>
					<li>Medication groups</li>
					<li>Relapse prevention groups</li>
					<li>Trauma therapy</li>
					<li>12-Step meetings – DDRA, CoDA, AA, NA, OA</li>
					<li>Co-dependency groups</li>
					<li>Adventure experiences</li>
				</ul>
			</div>			
			<div class="top-spacing">
				<div class="large-10 large-centered columns">
					<p>We support patients with an integrated, multi-disciplinary team. Primary therapists, consulting physicians, residential therapists, nurses, group leaders and others are all part of a patient’s treatment team. This team meets weekly to discuss the progress of each patient and to plan for his or her stability and recovery.</p>
				</div>				
			</div>
		</div>			
	</div>
</section>
<section class="spacing light-block last">
	<div class="row">
		<div class=" medium-centered medium-10 small-12 columns">
			<div class="border-box white-bg">
				<h2 class="alt-h2 text-center">Adventure Experiences</h2>
				<p>Adventure experiences and recreational activities range from light morning walks to intense hikes in the colorful desert and canyon landscape.The nearby Joshua Tree National Park provides a world-class hiking opportunity while offering time for mindfulness and anxiety relief. The Palm Springs Tramway is a popular choice because it offers a gentle, scenic ride up to a forest hiking spot that gives patients a unique low-impact experience of a beautiful mountain range.<br /><br /></p>
			</div>
			<div class="text-center offset-btn">
				<a href="/adventure-program/" class="button large radius"><span class="arrow">Explore our Adventures</span></a>
			</div>
		</div>
	</div>

	<div class="row">
		<div class=" medium-centered medium-10 small-12 columns">
		  <div class="border-box white-bg">
			<h2 class="alt-h2 text-center">Family Program</h2>
			  <p>Michael&rsquo;s House staff is committed to provide patients with a variety of tools, experiences and knowledge related to substance abuse and mental health recovery. Providing a three-day intensive family program is one way for families, partners and loved ones to share in this experience. This program enables families and supportive others to begin or continue in the healing process through validation, education, and support. <br /><br /></p>
			</div>
			<div class="text-center offset-btn">
				<a href="/family-program/" class="button large radius"><span class="arrow">Explore our Family Program</span></a>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>