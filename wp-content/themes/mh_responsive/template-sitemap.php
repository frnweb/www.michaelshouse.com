<?php
/*
Template Name: Sitemap
*/
?>
<?php get_header(); ?>
<section class="light-block standard-header">
  <div class="row">
    <div class="small-12 columns">
      <header id="page-id">
        <h1><?php the_title(); ?></h1>
        <?php get_template_part('library/includes/breadcrumbs'); ?>
      </header>
    </div>
  </div>
</section>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div id="content_block">
		<div style="padding-top:10px;" id="left_content">
			<div id="sitemap">
 
<div class="row last">
  <div class="medium-9 columns">
<h2 id="pages">Main Pages</h2>
<ul>
<?php
// Add pages you'd like to exclude in the exclude here
wp_list_pages( 
  array(
    'exclude' => '',
    'title_li' => '',
    'post_status'  => 'publish',
  )
);
?>
</ul>
 
<h2 id="posts">Sub Pages</h2>
<ul>
<?php
// Add categories you'd like to exclude in the exclude here
$cats = get_categories('exclude=');
foreach ($cats as $cat) {
	
  echo "<li><h3>".$cat->cat_name."</h3>";
  echo "<ul>";
  query_posts('posts_per_page=-1&cat='.$cat->cat_ID);
  while(have_posts()) {
    the_post();

      echo '<li><a href="'.get_permalink().'">'.get_the_title().'</a></li>';

  }
  echo "</ul>";
  echo "</li>";
}
?>
</ul>
</div>
<?php get_sidebar(); ?>
</div>

<?php wp_reset_query();?>
<?php endwhile; endif; ?>
<?php get_footer();?>