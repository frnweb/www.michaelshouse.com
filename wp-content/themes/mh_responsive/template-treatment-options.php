<?php
/*
Template Name: Treatment Options OLD
*/
?>


<?php get_header(); ?>
<section class="banner treatment-options">
	<h1 class="text-center">Proven Treatment Options<br><small>A Commitment to Care</small></h1>
	<?php include get_template_directory().'/social.php'; ?>
</section>
<section class="spacing">
	<div class="row" data-equalizer>
		<div class="large-4 small-12 columns">
			<div class="border-box">
				<h2 id="alcohol-rehab" class="alt-h2">Alcohol Rehab</h2>
				<p data-equalizer-watch>
					Effective alcohol rehabilitation programs provide a variety of services aimed at meeting the treatment goals and needs of the people they serve. At Michael's House, you can determine the pace for your recovery according to your readiness to heal or change.
				</p>
			</div>
			<div class="text-center treatment-btn">
				<a href="http://www.michaelshouse.com/alcohol-rehab" class="button large radius"><span class="arrow">Alcohol Rehab</span></a>
			</div>
		</div>
		<div class="large-4 small-12 columns">
			<div class="border-box">
				<h2 id="drug-rehab" class="alt-h2">Drug Rehab</h2>
				<p data-equalizer-watch>
					During your rehabilitation from drugs at Michael's House, we can help you stabilize physically and mentally so that you can heal and learn new techniques that will help you thrive in recovery. Each day we are assisting individuals in the process of establishing a foundation of recovery so they can function effectively in their families, workplaces, and communities.
				</p>
			</div>
			<div class="text-center treatment-btn">
				<a href="http://www.michaelshouse.com/drug-rehab" class="button large radius"><span class="arrow">Drug Rehab</span></a>
			</div>
		</div>
		<div class="large-4 small-12 columns">
			<div class="border-box">
				<h2 id="alcohol-detox" class="alt-h2">Alcohol Detox</h2>
				<p data-equalizer-watch>
					Alcohol detox at Michael's House is supervised by consulting physicians, and we have 24 hour nursing so that needs can be met as they arise. We use social model detox, meaning that patients will be able to interact with others and stabilize through gentle exercises as they allow their bodies to adjust to the absence of alcohol. Detox is the first step in a comprehensive treatment and recovery process.
				</p>
			</div>
			<div class="text-center treatment-btn">
				<a href="http://www.michaelshouse.com/alcohol-detox" class="button large radius"><span class="arrow">Alcohol Detox</span></a>
			</div>
		</div>
	</div>
</section>
<section class="cta-spacing light-block">
	<div class="row">
		<div class="small-12 columns text-center">
			<a href="http://www.michaelshouse.com/our-programs" class="button large round secondary"><span class="step">Step 3</span><span class="step-text">What is my Treatment Experience?</span></a>
			<p class="helper">Learn all you need to know through a few easy steps</p>
		</div>
	</div>
</section>
<section class="spacing">
	<div class="row" data-equalizer>
		<div class="large-4 small-12 columns">
			<div class="border-box">
				<h2 id="drug-detox" class="alt-h2">Drug Detox</h2>
				<p data-equalizer-watch>
					Our experienced staff of treatment professionals and consulting physicians will assist you through the drug detox process and on to the next phase of your treatment. We believe in a comprehensive approach that addresses the needs of the whole person in recovery. For more on what the complete treatment program would look like, please call us for more information.
				</p>
			</div>
			<div class="text-center treatment-btn">
				<a href="http://www.michaelshouse.com/drug-detox" class="button large radius"><span class="arrow">Drug Detox</span></a>
			</div> 
		</div>
		<div class="large-4 small-12 columns">
			<div class="border-box">
				<h2 id="dual-diagnosis" class="alt-h2">Dual Diagnosis</h2>
				<p data-equalizer-watch>
					Michael's House is nationally recognized for our integrated and evidence-based methods that have produced proven results for individuals with addiction and mental health disorders. According to an independent DDCAT assessment by a member of the Dartmouth Psychiatric Research Center, we are in the top 5% of addiction treatment programs for our ability to treat co-occurring disorders.
				</p>
			</div>
			<div class="text-center treatment-btn">
				<a href="http://www.michaelshouse.com/dual-diagnosis" class="button large radius"><span class="arrow">Dual Diagnosis</span></a>
			</div>
		</div>
		<div class="large-4 small-12 columns">
			<div class="border-box">
				<h2 id="intervention" class="alt-h2">Intervention</h2>
				<p data-equalizer-watch>
					An intervention isn't just a meeting. It's the strategic commitment to a loved one's recovery process, and it's just as much about getting the family stabilized as the person suffering from the addiction. When you want to help someone you love who is not yet seeking treatment, you can trust us to connect you to professional interventionist whose expertise matches your situation. You are one phone call away from getting the help of a family mediator who can restore balance to your family and help your loved one recognize the need for healing and restoration. 
				</p>
			</div>
			<div class="text-center treatment-btn">
				<a href="http://www.michaelshouse.com/intervention" class="button large radius"><span class="arrow">Intervention</span></a>
			</div>
		</div>
	</div>
</section>
<section class="light-block spacing last">
	<div class="row">
		<?php
		  $children = wp_list_pages('title_li=&child_of='.$post->ID.'&echo=0');
		  if ($children) { ?>
				<h2 class="alt-h2 text-center">Further Reading</h2>
				<div class="large-6 large-centered medium-8 medium-centered small-12 columns">  
				  <ul class="reading medium-block-grid-2 small-block-grid-1">
				  <?php echo $children; ?>
				  </ul>
				</div>
		<?php } ?>		
	</div>
</section>
<?php get_footer(); ?>