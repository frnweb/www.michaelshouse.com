<?php
/*
Template Name: Home Page
*/
?>


<?php get_header(); ?>

<div class="welcome">
	<div class="row">
		<div class="small-12 columns">
			<h1 class="text-center">You're Just Steps Away from<br>Changing Your Life</h1>
		</div>
	</div>
</div>
<div class="row">
	<div class="large-8 large-centered small-11 small-centered columns step-1 text-center">
		<h2 class="alt-h2">Get Started Here</h2>
		<p class="handwriting">5 Things People Ask about Michael’s House Treatment</p>
		<a href="http://www.michaelshouse.com/why-it-works" class="button large round secondary"><span class="step">Step 1</span><span class="step-text">Why Should I Choose Michael’s House for Treatment?</span></a>
		<p class="helper">Learn all you need to know through a few easy steps</p>
	</div>
</div>


<section class="light-block home-entrance-sketch">
	<div class="row" data-equalizer>
		<div class="medium-6 columns" data-equalizer-watch>
			<h2 class="text-center alt-h2">Real Stories</h2>
			<div class="flex-video">
				<iframe width="1280" height="720" src="//www.youtube.com/embed/te4DdG754f4?rel=0" frameborder="0" allowfullscreen></iframe>
			</div>
			<div class="text-center">
				<a href="http://www.michaelshouse.com/testimonials" class="alt-link">Hear from others in recovery</a>
			</div> 
		</div>
		<div class="medium-6 columns" data-equalizer-watch>
			<h2 class="text-center alt-h2">Most Insurance Accepted</h2>
			<div class="text-center">
				<img src="wp-content/themes/mh_responsive/style/images/insurance-home.png" width="588px" height="275px">
				<p class="orange">We can help you navigate insurance coverage</p>
				<a href="http://www.michaelshouse.com/insurance" class="button large radius"><span class="arrow">Check if you're covered</span></a>
			</div>
		</div>
	</div> 
	<div class="row">
		<div class="small-12 columns">
			<h2 class="text-center alt-h2">Our Locations<br><small>Providing All Levels of Care</small></h2>
			<ul class="large-block-grid-4 medium-block-grid-2 small-block-grid-1 location">
				<li class="text-center">
					<h3>Stabilization Center</h3>
					<div class="frame">
						<a href="http://www.michaelshouse.com/locations/michaels-house-north/"><img src="wp-content/themes/mh_responsive/style/images/stabl.jpg" width="466px" height="312px"></a>
					</div>
				</li>
				<li class="text-center">
					<h3>Women's Center</h3>
					<div class="frame">
						<a href="http://www.michaelshouse.com/locations/michaels-house-south"><img src="wp-content/themes/mh_responsive/style/images/womens.jpg" width="466px" height="312px"></a>
					</div>
				</li>
				<li class="text-center">
					<h3>Men's Center</h3>
					<div class="frame">
						<a href="http://www.michaelshouse.com/locations/mens-center"><img src="wp-content/themes/mh_responsive/style/images/mens.jpg" width="466px" height="310px"></a>
					</div>
				</li>
				<li class="text-center">
					<h3>Outpatient Center</h3>
					<div class="frame">
						<a href="http://www.michaelshouse.com/locations/outpatient-program"><img src="wp-content/themes/mh_responsive/style/images/outpatient.jpg" width="466px" height="310px"></a>
					</div>
				</li>
			</ul>
		</div>  
	</div>
</section>
<section class="trusted-treatment">
<div class="row">
	<div class="large-4 columns">
		<h1 class="text-center">Trusted Treatment</h1>
	</div>
	<div class="large-3 large-uncentered medium-8 medium-centered columns text-center">
		<ul class="small-block-grid-3">
			<li><img src="wp-content/themes/mh_responsive/style/images/badge.png" width="80px" height="78px"></li>
			<li><img src="wp-content/themes/mh_responsive/style/images/caarf.png" width="75px" height="69px"></li>
			<li><img src="wp-content/themes/mh_responsive/style/images/naatp.png" width="191px" height="67px"></li>
		</ul>
	</div>
	<div class="large-5 columns">
		<p class="text-center handwriting">Take a step to change your life<br>
		<span class="phone-cta"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Page (Trusted Treatment Bar)"]'); ?></span></p>
	</div>
</div>
</section>
<section class="spacing last light-block">
	<div class="row bottom-spacing">
		<div class="small-12 columns">
			<h2 class="text-center alt-h2">Get Help Today<br><small>Discover More About Our Programs</small></h2>
		</div>
		<div class="medium-4 columns text-center">
			<a href="#" class="button large radius drop" data-dropdown="drop-1"><span class="arrow-drop">Addiction Treatment</span></a>
				<ul id="drop-1" class="small f-dropdown" data-dropdown-content>
				  <li><a href="http://www.michaelshouse.com/alcohol-rehab">Alcohol Rehab</a></li>
				  <li><a href="http://www.michaelshouse.com/drug-rehab">Drug Rehab</a></li>
				  <li><a href="http://www.michaelshouse.com/prescription-drug-addiction/">Prescription Drug Abuse</a></li>
				</ul>
			<p>Trusted treatment for proven results.</p>	
		</div>
		<div class="medium-4 columns text-center">
			<a href="#" class="button large radius drop" data-dropdown="drop-2"><span class="arrow-drop">Co-Occurring Disorders</span></a>
				<ul id="drop-2" class="small f-dropdown" data-dropdown-content>
				  <li><a href="http://www.michaelshouse.com/dual-diagnosis/">Dual Diagnosis Treatment</a></li>
				  <li><a href="http://www.michaelshouse.com/dual-diagnosis/depression-and-addiction/">Depression & Addiction</a></li>
				  <li><a href="http://www.michaelshouse.com/drug-addiction/">Anxiety & Addiction</a></li>
				</ul>
			<p>We treat addiction and beyond.</p>	
		</div>
		<div class="medium-4 columns text-center">
			<a href="#" class="button large radius drop" data-dropdown="drop-3"><span class="arrow-drop">Treatment Programs</span></a>
				<ul id="drop-3" class="small f-dropdown" data-dropdown-content>
				  <li><a href="http://www.michaelshouse.com/drug-detox">Drug Detox</a></li>
				  <li><a href="http://www.michaelshouse.com/addiction-treatment/outpatient">Outpatient Treatment</a></li>
				</ul>
			<p>We provide help with a variety of treatment options.</p>	
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns">
			<h2 class="text-center alt-h2">Our Caring Staff<br><small>Meet our team of industry-leading professionals</small></h2>
			<div class="staff">
				<ul class="small-block-grid-2 medium-block-grid-3 large-block-grid-5">
					<?php $the_query = new WP_Query( 'post_type=staff&posts_per_page=6&staff_location=homepage&order=ASC' );
					while ( $the_query->have_posts() ) : $the_query->the_post();?>
					<li>
						<a href="<?php echo the_permalink(); ?>" class="headshot-hover">
							<div class="headshot">
								<?php echo get_the_post_thumbnail();?>
							</div>
							<h3 id="staff-title-hover" class="text-center"><small><strong><?php echo get_the_title(); ?></strong><br><?php echo get_post_meta(get_the_ID(), 'frothy_staff_role', true); ?></small></h3>
						</a>						
					</li>
					<?php endwhile; wp_reset_postdata(); ?>		

			</ul>
			<div class="text-center">
				<a href="http://www.michaelshouse.com/staff" class="alt-link">Learn more about your dedicated recovery team</a>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>