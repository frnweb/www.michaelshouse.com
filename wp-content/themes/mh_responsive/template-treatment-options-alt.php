<?php
/*
Template Name: Treatment Options Alt
*/
?>


<?php get_header(); ?>
<section class="banner treatment-options">
	<h1 class="text-center">Proven Treatment Options<br><small>A Commitment to Care</small></h1>
	<div class="row" data-equalizer>
		<div class="large-6 large-uncentered medium-8 medium-centered columns" data-equalizer-watch>
			<div class="flex-video">
				<iframe width="1280" height="720" src="//www.youtube.com/embed/l3wAu_R4ako?rel=0" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
		<div class="large-6 columns" data-equalizer-watch>
			<p>The treatment program at Michael's House is nationally recognized for our integrated and evidence-based methods that have produced proven results for individuals with addiction and mental health disorders. According to an independent DDCAT assessment by a member of the Dartmouth Psychiatric Research Center, we are in the top 5% of addiction treatment programs for our ability to treat <a href="http://www.michaelshouse.com/dual-diagnosis">co-occurring disorders</a>.</p>
			<div class="text-center">
				<a href="http://www.michaelshouse.com/our-programs" class="button large round secondary"><span class="step">Step 3</span><span class="step-text">What is my treatment experience?</span></a>
				<p class="helper">Learn all you need to know through a few easy steps</p>
			</div>				
		</div>
	</div>
	<?php include get_template_directory().'/social.php'; ?>
</section>
<section>
	<div class="border-block spacing">
		<div class="row" data-equalizer>
			<div class="medium-6 small-12 columns">
				<div class="border-box">
					<h2 id="alcohol-rehab" class="alt-h2">Alcohol Rehab</h2>
					<p data-equalizer-watch>
						Effective alcohol rehabilitation programs provide a variety of services aimed at meeting the treatment goals and needs of the people they serve. At Michael's House, you can determine the pace for your recovery according to your readiness to heal or change.
					</p>
				</div>
				<div class="text-center treatment-btn">
					<a href="http://www.michaelshouse.com/alcohol-rehab" class="button large radius"><span class="arrow">Alcohol Rehab</span></a>
				</div>
			</div>	
			<div class="medium-6 small-12 columns">
				<div class="border-box">
					<h2 id="drug-rehab" class="alt-h2">Drug Rehab</h2>
					<p data-equalizer-watch>
						During your rehabilitation from drugs at Michael's House, we can help you stabilize physically and mentally so that you can heal and learn new techniques that will help you thrive in recovery. Each day we are assisting individuals in the process of establishing a foundation of recovery so they can function effectively in their families, workplaces, and communities.
					</p>
				</div>
				<div class="text-center treatment-btn">
					<a href="http://www.michaelshouse.com/drug-rehab" class="button large radius"><span class="arrow">Drug Rehab</span></a>
				</div>
			</div>
		</div>
		<div class="row" data-equalizer>
			<div class="medium-6 small-12 columns">
				<div class="border-box">
					<h2 id="alcohol-detox" class="alt-h2">Alcohol Detox</h2>
					<p data-equalizer-watch>
						Alcohol detox at Michael's House is supervised by consulting physicians, and we have 24 hour nursing so that needs can be met as they arise. We use social model detox, meaning that patients will be able to interact with others and stabilize through gentle exercises as they allow their bodies to adjust to the absence of alcohol. Detox is the first step in a comprehensive treatment and recovery process.
					</p>
				</div>
				<div class="text-center treatment-btn">
					<a href="http://www.michaelshouse.com/alcohol-detox" class="button large radius"><span class="arrow">Alcohol Detox</span></a>
				</div>
			</div>		
			<div class="medium-6 small-12 columns">
				<div class="border-box">
					<h2 id="drug-detox" class="alt-h2">Drug Detox</h2>
					<p data-equalizer-watch>
						Our experienced staff of treatment professionals and consulting physicians will assist you through the drug detox process and on to the next phase of your treatment. We believe in a comprehensive approach that addresses the needs of the whole person in recovery. For more on what the complete treatment program would look like, please call us for more information.
					</p>
				</div>
				<div class="text-center treatment-btn">
					<a href="http://www.michaelshouse.com/drug-detox" class="button large radius"><span class="arrow">Drug Detox</span></a>
				</div> 
			</div>
		</div>

		<div class="row" data-equalizer>
			<div class="medium-6 small-12 columns">
				<div class="border-box">
					<h2 id="outpatient" class="alt-h2">Outpatient</h2>
					<p data-equalizer-watch>
						Our outpatient program features three levels that build on the levels system used in our residential program. Patients begin with three hours of education per day, focusing on developing a schedule for a life of sobriety, increasing their recovery skill sets, continuing in group therapy, and gaining valuable relapse prevention awareness.
					</p>
				</div>
				<div class="text-center treatment-btn">
					<a href="http://www.michaelshouse.com/locations/outpatient-program" class="button large radius"><span class="arrow">Outpatient</span></a>
				</div>
			</div>		
			<div class="medium-6 small-12 columns">
				<div class="border-box">
					<h2 id="mental-health" class="alt-h2">Mental Health</h2>
					<p data-equalizer-watch>
						At Michael’s House, we provide care for people who have both mental illness issues and addiction issues. Often, people with mental illnesses need to obtain targeted help from dedicated professionals in formal treatment programs. Using personally tailored programs our therapists work closely with clients, ensuring that their wants and needs are respected, and that they aren’t pushed into situations in which they don’t feel comfortable.
					</p>
				</div>
				<div class="text-center treatment-btn">
					<a href="http://www.michaelshouse.com/mental-health" class="button large radius"><span class="arrow">Mental Health</span></a>
				</div> 
			</div>
		</div>
	</div>
</section>
<section class="light-block spacing last">
	<div class="row">
		<?php
		  $children = wp_list_pages('title_li=&child_of='.$post->ID.'&echo=0');
		  if ($children) { ?>
				<h2 class="alt-h2 text-center">Further Reading</h2>
				<div class="large-6 large-centered medium-8 medium-centered small-12 columns">  
				  <ul class="reading medium-block-grid-2 small-block-grid-1">
				  <?php echo $children; ?>
				  </ul>
				</div>
		<?php } ?>		
	</div>
</section>
<?php get_footer(); ?>