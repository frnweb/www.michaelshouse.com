<div id="steps_widget" class="widget widget_steps_widget">
	<h2 class="alt-h2">5 Things People Ask About Michael’s House Treatment</h2>
	<ol>
		<li><a href="http://www.michaelshouse.com/why-it-works" class="alt-link">Why should I choose Michael's House for treatment?</a></li>
		<li><a href="http://www.michaelshouse.com/treatment-program" class="alt-link">What are my treatment options?</a></li>
		<li><a href="http://www.michaelshouse.com/our-programs" class="alt-link">What is my treatment experience?</a></li>
		<li><a href="http://www.michaelshouse.com/locations" class="alt-link">Where are we located?</a></li>
		<li><a href="http://www.michaelshouse.com/treatment-admissions" class="alt-link">What does the admissions process look like?</a></li>
		<!-- <li><a href="http://www.michaelshouse.com/contact-us" class="alt-link">Contact us now to get started</a></li> -->
	</ol>
	<div class="text-center">
		<a href="http://www.michaelshouse.com/contact-us" class="button small round secondary"><span class="step-text">Contact us now to get started</span></a>
	</div>
</div>