<?php
  $children = wp_list_pages('title_li=&child_of='.$post->ID.'&echo=0');
  if ($children) { ?>
  <div id="further-side" class="widget">
	  <h2 class="alt-h2">Further Reading</h2>
	  <ul class="reading large-block-grid-1 medium-block-grid-2 small-block-grid-1">
	  <?php echo $children; ?>
	  </ul>
   </div>
<?php } ?>