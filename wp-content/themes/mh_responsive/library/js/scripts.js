// jQuery(document).ready(function($) {
	//Alternating Rows
	$("#subpage .sub_item:even").addClass("alt");
	
	//Content Formatting
	//Accordion
	$('div.accordion> div').hide();  
	  $('div.accordion> h3').click(function() {
	    $(this).next('div').slideToggle('fast')
	    .siblings('div:visible').slideUp('fast');
	});
	
	//Tabs
	$('.tab-menu li a').click(function(event) {
	    event.preventDefault();
	    $('.dynamic').hide();
	    $('.tab-menu li a').removeClass('current');
	    var parent = $(event.target).parent();
	    $('.dynamic.' + parent.attr('class')).fadeIn();
	    parent.find('a').addClass('current');
	});
	
	//Equal Columns
	var max_height = 0;
	$("div.third").each(function(){
	    if ($(this).height() > max_height) { max_height = $(this).height(); }
	});
	$("div.third").height(max_height);
	
	//Responsive Menu Sidr
	$('#responsive-menu-button').sidr({
		name: 'sidr-main',
		source: '#mobile-title, .menu-main-nav-container'
	});

	
//Reveal subnav
	$('#menuIcon').click(function() {
		showHideMenu();
	});

function showHideMenu() {
	$('#bigNav').fadeToggle(150);
	$('#menuIcon').toggleClass('x');
}
function hideMenu() {
	$('#bigNav').fadeOut(150);
	$('#menuIcon').removeClass('x');
}
//smooth scrolling
// $(function() {
//   $('a[href*=#]:not([href=#])').click(function() {
//     if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
//       var target = $(this.hash);
//       target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
//       if (target.length) {
//         $('html,body').animate({
//           scrollTop: target.offset().top
//         }, 1000);
//         return false;
//       }
//     }
//   });
// });	

// });