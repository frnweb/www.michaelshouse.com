<?php
/*
Template Name: Staff Page
*/
?>
<?php get_header(); ?>
<section class="light-block standard-header">
  <div class="row">
    <div class="small-12 columns">
      <header id="page-id">
        <h1><?php the_title(); ?></h1>
        <?php get_template_part('library/includes/breadcrumbs'); ?>
        <?php include get_template_directory().'/social.php'; ?>
      </header>
    </div>
  </div>
</section>

<section class="border-block">
<div class="row">
   <div class="large-6 large-centered medium-8 medium-centered small-12 columns text-center">
    <h2>Group CEO, Facility CEO and Medical Director</h2>
    <ul class="medium-block-grid-2 small-block-grid-1">
      <li>
        <div class="headshot">
          <a href="<?php echo get_page_link('3219'); ?>"><?php echo get_the_post_thumbnail( '3219', $size, $attr ); ?></a> 
        </div>
        <h3 class="text-center"><small><strong><?php echo get_the_title( '3219' ); ?></strong><br> 
        <?php echo get_post_meta('3219', 'frothy_staff_role', true); ?></small></h3>
      </li>
      <li>
        <div class="headshot">
          <a href="<?php echo get_page_link('3242'); ?>"><?php echo get_the_post_thumbnail( '3242', $size, $attr ); ?></a> 
        </div>
        <h3 class="text-center"><small><strong><?php echo get_the_title( '3242' ); ?> 
        </strong><br> 
        <?php echo get_post_meta('3242', 'frothy_staff_role', true); ?></small></h3>
      </li>
    </ul>
  </div>
</div>
</section>
<section class="border-block">
  <div class="row">
    <div class="large-6 large-centered medium-8 medium-centered small-12 columns text-center">
      <h2>Clinical Managers</h2>
      <ul class="medium-block-grid-2 small-block-grid-1">
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('3815'); ?>"><?php echo get_the_post_thumbnail( '3815', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '3815' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('3815', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('3797'); ?>"><?php echo get_the_post_thumbnail( '3797', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '3797' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('3797', 'frothy_staff_role', true); ?></small></h3>
        </li>
      </ul>
    </div>
  </div>
</section>
<section class="border-block">
  <div class="row">
    <div class="small-12 columns text-center">
      <h2>Clinical Staff</h2>
      <ul class="large-block-grid-6 medium-block-grid-4 small-block-grid-1">
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('4360'); ?>"><?php echo get_the_post_thumbnail( '4360', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '4360' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('4360', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('4293'); ?>"><?php echo get_the_post_thumbnail( '4293', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '4293' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('4293', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('4291'); ?>"><?php echo get_the_post_thumbnail( '4291', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '4291' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('4291', 'frothy_staff_role', true); ?></small></h3>
        </li>
          <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('4289'); ?>"><?php echo get_the_post_thumbnail( '4289', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '4289' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('4289', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('8756'); ?>"><?php echo get_the_post_thumbnail( '8756', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '8756' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('8756', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('3835'); ?>"><?php echo get_the_post_thumbnail( '3835', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '3835' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('3835', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('8755'); ?>"><?php echo get_the_post_thumbnail( '8755', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '8755' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('8755', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('3823'); ?>"><?php echo get_the_post_thumbnail( '3823', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '3823' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('3823', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('3821'); ?>"><?php echo get_the_post_thumbnail( '3821', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '3821' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('3821', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('3817'); ?>"><?php echo get_the_post_thumbnail( '3817', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '3817' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('3817', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('3813'); ?>"><?php echo get_the_post_thumbnail( '3813', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '3813' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('3813', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('3807'); ?>"><?php echo get_the_post_thumbnail( '3807', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '3807' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('3807', 'frothy_staff_role', true); ?></small></h3>
        </li>
        
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('3805'); ?>"><?php echo get_the_post_thumbnail( '3805', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '3805' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('3805', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('3803'); ?>"><?php echo get_the_post_thumbnail( '3803', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '3803' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('3803', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('3799'); ?>"><?php echo get_the_post_thumbnail( '3799', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '3799' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('3799', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('3793'); ?>"><?php echo get_the_post_thumbnail( '3793', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '3793' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('3793', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('3788'); ?>"><?php echo get_the_post_thumbnail( '3788', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '3788' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('3788', 'frothy_staff_role', true); ?></small></h3>
        </li>
        
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('3784'); ?>"><?php echo get_the_post_thumbnail( '3784', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '3784' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('3784', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('3780'); ?>"><?php echo get_the_post_thumbnail( '3780', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '3780' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('3780', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('8754'); ?>"><?php echo get_the_post_thumbnail( '8754', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '8754' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('8754', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('8752'); ?>"><?php echo get_the_post_thumbnail( '8752', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '8752' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('8752', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('8750'); ?>"><?php echo get_the_post_thumbnail( '8750', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '8750' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('8750', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('3244'); ?>"><?php echo get_the_post_thumbnail( '3244', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '3244' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('3244', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('3238'); ?>"><?php echo get_the_post_thumbnail( '3238', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '3238' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('3238', 'frothy_staff_role', true); ?></small></h3>
        </li>
      </ul>
    </div>
  </div>
</section>
<section class="border-block">
  <div class="row">
    <div class="large-6 large-centered medium-10 medium-centered small-12 columns text-center">
      <ul class="medium-block-grid-2 small-block-grid-1">
        <h2>Medical Support Staff</h2>        
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('4085'); ?>"><?php echo get_the_post_thumbnail( '4085', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '4085' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('4085', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('3830'); ?>"><?php echo get_the_post_thumbnail( '3830', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '3830' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('3830', 'frothy_staff_role', true); ?></small></h3>
        </li>
      </ul>
    </div>
  </div>
</section>
<section class="border-block last">
  <div class="row">
    <div class="large-10 large-centered small-12 columns text-center">
      <h2>Facility Operations</h2>
      <ul class="medium-block-grid-4 small-block-grid-1">
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('8757'); ?>"><?php echo get_the_post_thumbnail( '8757', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '8757' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('8757', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('3795'); ?>"><?php echo get_the_post_thumbnail( '3795', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '3795' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('3795', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('8753'); ?>"><?php echo get_the_post_thumbnail( '8753', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '8753' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('8753', 'frothy_staff_role', true); ?></small></h3>
        </li>
        <li>
          <div class="headshot">
            <a href="<?php echo get_page_link('8751'); ?>"><?php echo get_the_post_thumbnail( '8751', $size, $attr ); ?></a> 
          </div>
          <h3 class="text-center"><small><strong><?php echo get_the_title( '8751' ); ?> 
          </strong><br> 
        <?php echo get_post_meta('8751', 'frothy_staff_role', true); ?></small></h3>
        </li>
      </ul>
    </div>
  </div>
</section>
<?php get_footer(); ?>