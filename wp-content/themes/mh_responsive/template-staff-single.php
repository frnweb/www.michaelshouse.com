<?php
/*
Template Name: Staff Single
*/
?>
<?php get_header(); ?>
<section class="light-block standard-header">
	<div class="row">
		<div class="small-12 columns">
			<header id="page-id">
				<h1><?php the_title(); ?></h1>
				<h2><small><?php echo get_post_meta(get_the_id(), 'frothy_staff_role', true); ?></small></h2>
				<?php get_template_part('library/includes/breadcrumbs'); ?>
			</header>
		</div>
	</div>
	<?php include get_template_directory().'/social.php'; ?>
</section>
<div class="row last">
	<section role="main" class="large-9 columns">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
	<article>
		<div class="headshot left">
			<?php
			if ( has_post_thumbnail() ) {
				the_post_thumbnail( 'large' );
			}
			?>
		</div>
		<?php the_content(); ?>
	</article>
	<?php endwhile; endif; ?>
	</section>
<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>