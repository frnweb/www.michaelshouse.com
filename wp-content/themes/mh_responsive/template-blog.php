<?php
/*
Template Name: Blog
*/  
?>
<?php get_header(); ?>
<section class="light-block standard-header">
	<div class="row">
		<div class="small-12 columns">
			<header id="page-id">
				<h1><?php the_title(); ?></h1>
				<?php get_template_part('library/includes/breadcrumbs'); ?>
			</header>
		</div>
	</div>
	<?php include get_template_directory().'/social.php'; ?>
</section>
<div class="row last">
<section role="main" class="large-9 columns">
<?php query_posts('post_type=post&posts_per_page=10&paged='.$paged);?>
	<?php if (have_posts()) : while  (have_posts()) : the_post(); ?>
	<article class="blog">
		<?php
		if ( has_post_thumbnail() ) {?>
			<a href="<?php the_permalink();?>"><?php the_post_thumbnail( 'large' );?></a>
		<?php } ?>
		<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
		<div class="postmeta">
			<?php the_time('F jS, Y') ?> | Posted In: <?php the_category(', ') ?> 
		</div><!-- end meta -->
		<?php the_excerpt();?>
		<a href="<?php the_permalink();?>" class="alt-link">Keep Reading</a>
	</article><!-- end blog -->	
	<hr>
<?php endwhile; ranklab_pagination(); endif; ?>

</section>
<?php get_sidebar(); ?>
</div> <!-- #main -->
<?php get_footer(); ?>