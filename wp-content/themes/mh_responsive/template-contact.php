<?php
/*
Template Name: Contact Us
*/
?>

<?php get_header(); ?>
<section class="banner contact-us">
	<h1 class="text-center">Contact us<br><small>100% Confidential</small></h1>
	<?php include get_template_directory().'/social.php'; ?>
</section>
<section class="spacing">
	<div class="row">
		<div class="large-6 columns text-center step-5">			
			<p class="handwriting">Get more answers about your options</p>
			<span class="orange large radius phone-cta medium-12 columns"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Contact Page"]'); ?></span>
			<ul class="button-group radius contact-btns">
				<li><a onclick="window.open('http://www.livehelpnow.net/lhn/TicketsVisitor.aspx?lhnid=14160','Ticket','left=' + (screen.width - 550-32) / 2 + ',top=50,scrollbars=yes,menubar=no,height=550,width=450,resizable=yes,toolbar=no,location=no,status=no');return false;" class="button large" id="lhnEmailButton">Email us</a></li>
				<li><a onclick="OpenLHNChat();return false;" class="button large" id="lhnchatimg">Chat with us</a></li>
			</ul>			
		</div>
		<div class="large-6 columns text-center">
			<img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/insurance-block.jpg" alt="insurance logos">
		</div>
	</div>
</section>
<section class="trusted-treatment light-block">
<div class="row">
	<div class="large-5 large-offset-1 columns">
		<h1 class="orange large-text-left small-text-center">Accredited Treatment</h1>
	</div>
	<div class="large-5 large-pull-1 medium-10 medium-pull-1 columns text-center">
		<ul class="small-block-grid-3">
			<li><img src="<?php echo get_template_directory_uri();?>/style/images/badge.png"></li>
			<li><img src="<?php echo get_template_directory_uri();?>/style/images/caarf.png"></li>
			<li><img src="<?php echo get_template_directory_uri();?>/style/images/naatp.png"></li>
		</ul>
	</div>
</div>
</section>
<section class="spacing last">
	<div class="row">
		<div class="small-12 columns map">
			<div class="flex-video">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3313.6073890915536!2d-116.54611479999998!3d33.8482343!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80db1bdb180b2379%3A0x40c14682109875ad!2sMichael&#39;s+House!5e0!3m2!1sen!2sus!4v1436291413068" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>				
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>