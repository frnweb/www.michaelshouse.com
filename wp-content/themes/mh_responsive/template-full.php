<?php
/*
Template Name: Full No sidebar
*/
?>
<?php get_header(); ?>
<section class="light-block standard-header">
	<div class="row">
		<div class="small-12 columns">
			<header id="page-id">
				<h1><?php the_title(); ?></h1>
				<?php get_template_part('library/includes/breadcrumbs'); ?>
			</header>
		</div>
	</div>
	<?php include get_template_directory().'/social.php'; ?>
</section>
<div class="row last">
	<section role="main" class="large-12 columns">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
	<article>
		<?php
		if ( has_post_thumbnail() ) {
			the_post_thumbnail( 'large' );
		}
		?>
		<?php the_content(); ?>
	</article>
	<?php endwhile; endif; ?>
	</section>
</div>
<?php get_footer(); ?>